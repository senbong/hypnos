from __future__ import division

import random
import cPickle as pickle
from itertools import cycle
from collections import defaultdict

import numpy as np
import matplotlib.pyplot as plt

from . import coroutine, AbstractSelection, ObjectiveVariableMap



__all__ = [ "Individual", 
            "DominationSelection" ]



class Individual(ObjectiveVariableMap):
    """Individual class used in domination-based selection method.

    Parameters
    ----------
    minimization: bool
        Maximization or minimization problem.
    """

    def __init__(self, minimization):
        super(Individual, self).__init__(minimization)
        self.rank = -1
        self.count = 0
        self.crowding_distance = 0
        self._hash_id = hash(self)

    
    @property
    def hash_id(self):
        return self._hash_id


    def dominate(self, other):
        """Check whether self dominate other

        Parameter
        ---------
        other: Individual objective
            other individual to be compared.
        """
        if self.minimization == other.minimization:
            self_obj, other_obj = np.array(self.objective), np.array(other.objective)
            if self.minimization:
                return np.all(self_obj <= other_obj) and np.any(self_obj != other_obj)
            else:
                return np.all(self_obj >= other_obj) and np.any(self_obj != other_obj)



class DominationSelection(AbstractSelection):
    """Domination-based multi-objective environmental selection

    Parameters
    ----------
    objective_num: int
        Number of objectives

    population_size: int
        Number of individual solutions in the population

    minimization: bool
        Minimization or maximization problem

    References
    ----------
    .. [1] Deb, K., Pratap, A., Agarwal, S., & Meyarivan, T. A. M. T. (2002). A 
           fast and elitist multiobjective genetic algorithm: NSGA-II. 
           Evolutionary Computation, IEEE Transactions on. 
    """
    
    def __init__(self, nobj, population_size=100, minimization=True):
        self.objective_num = nobj
        self.minimization  = minimization
        self.population = list()
        self.offspring  = list()
        self._population_size = population_size

        # start evolution coroutine
        self._evals_num = 0
        self.evolution  = self._evolution()


    @property
    def population_size(self):
        return self._population_size


    @property
    def evals_num(self):
        return self._evals_num
    
    
    @property
    def generation_num(self):
        return self.evals_num//self.population_size


    def _nondominated_sorting(self, population):
        """Non-dominated sorting algorithm

        Parameter
        ---------
        population: list of Individual objects
            List of Individual objects to be sorted.
        """
        fronts = [[]]
        dominated_individuals = defaultdict(list)
        for p in population:
            p.count = 0
            for q in population:
                if p.dominate(q):
                    dominated_individuals[p.hash_id].append(q)
                elif q.dominate(p):
                    p.count += 1
            if p.count == 0:
                p.rank = 1
                fronts[0].append(p)

        front_count = 0
        while len(fronts[front_count]) != 0:
            next_front = list()
            for p in fronts[front_count]:
                for q in dominated_individuals[p.hash_id]:
                    q.count -= 1
                    if q.count == 0:
                        q.rank = front_count + 1
                        next_front.append(q)
            front_count += 1
            fronts.append([])
            fronts[front_count] = next_front
        return fronts
    
    
    def _crowding_distance(self, front):
        """Crowding distance assignment

        Parameter
        ---------
        front: list of Individuals
            Individuals in the same front
        """
        for ind in front:
            ind.crowding_distance = 0
        
        for i in range(self.objective_num):
            front.sort(lambda x,y: cmp(x.objective[i], y.objective[i]))
            front[0].crowding_distance  = np.finfo(float).max
            front[-1].crowding_distance = np.finfo(float).max
            for j in range(1, len(front) - 1):
                front[j].crowding_distance += (front[j+1].objective[i] - front[j-1].objective[i])


    def _update_population(self):
        """Environmental selection
        """
        combine_population = self.population + self.offspring
        fronts = self._nondominated_sorting(combine_population)
        self.population, self.offspring = list(), list()
        front_count = 0
        while len(self.population) + len(fronts[front_count]) <= self.population_size:
            self.population.extend(fronts[front_count])
            front_count += 1
        
        self._crowding_distance(fronts[front_count])
        fronts[front_count].sort(key=lambda x:x.crowding_distance, reverse=True)
        self.population.extend(fronts[front_count][:self.population_size-len(self.population)])
        
        
    @coroutine
    def _evolution(self):
        """Implement evolutionary process using coroutine
        """
        try:
            while True:
                while len(self.population) < self.population_size:
                    variable, objective = yield
                    ind = Individual(self.minimization)
                    ind.objective = objective
                    ind.variable  = variable
                    self.population.append(ind)
                    self._evals_num += 1
                    
                while len(self.offspring) < self.population_size:
                    variable, objective = yield
                    ind = Individual(self.minimization)
                    ind.objective = objective
                    ind.variable  = variable
                    self.offspring.append(ind)
                    self._evals_num += 1

                self._update_population()
        except GeneratorExit:
            print("==== Exit Domination Selection ====")
        except:
            print("Unexpected Error!!!")


    def feed(self, variable, objective):
        """Feed the data into the selection operator

        Parameters
        ----------
        variable: any type except None
            Parameters used to obtain the objective values
            
        objective: list of floats, or numpy array
            Objective vectors
        """
        if np.array(objective).size != self.objective_num:
            raise ValueError("Incorrect objective dimension.")

        if variable is None:
            raise ValueError("Variable is None.")
            
        self.evolution.send((variable, objective))
        
    
    def _tournament(self, i, j):
        """Tournament used in mating selection

        Parameters
        ----------
        i: int
            Index of the first parent

        j: int
            Index of the second parent
        """
        if self.population[i].rank < self.population[j].rank:
            return i
        elif self.population[i].rank > self.population[j].rank:
            return j
        elif self.population[i].crowding_distance > self.population[j].crowding_distance:
            return i
        else:
            return j


    def mating_index(self, parent_size=2):
        """Get the mating index used in mating selection

        Parameter
        ---------
        parent_size: int
            Number of parent indices to be returned by the function

        Return
        ------
        list of int
        """
        if parent_size > self.population_size:
            raise ValueError("Parent size must not exceed population size.")

        parent_indices = list()
        while len(parent_indices) < parent_size:
            i,j = random.sample(range(self.population_size), 2)
            index = self._tournament(i, j)
            if index not in parent_indices:
                parent_indices.append(index)
        return parent_indices

    
    def plot(self, dims=(0, 1), xlabel=None, ylabel=None, **kwargs):
        """Plot the Pareto front in 2D graph

        Parameters:
            dims: tuple of ints
                Objective indices to be plotted.

            xlabel: str
                X-axis label

            ylabel: str
                Y-axis label

            kwargs: keyword arguments
                Additional keyword arguments to customize the plot
        """
        if any(d >= self.objective_num or not isinstance(d, int) for d in dims):
            raise ValueError("Dimensions must be integer values within [0,{})".format(self.objective_num))

        if len(dims) != 2:
            raise ValueError("Size of dims must equal to 2.")
        
        if not kwargs:
            fronts =  self._nondominated_sorting(self.population)
            colors = cycle(['ko', 'bo', 'ro', 'mo', 'go', 'co', 'yo'])
            for front in fronts:
                if len(front):
                    front = [[ind.objective[d] for d in dims] for ind in front]
                    f1, f2 = zip(*front)
                    plt.plot(f1, f2, next(colors))
        else:
            x_array = [p.objective[dims[0]] for p in self.population if p.is_initialized()]
            y_array = [p.objective[dims[1]] for p in self.population if p.is_initialized()]
            plt.plot(x_array, y_array, **kwargs)

        # display plot
        xlabel = "$f_{}$".format(dims[0]+1) if xlabel is None else xlabel
        ylabel = "$f_{}$".format(dims[1]+1) if ylabel is None else ylabel
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.show()    
    

    def save(self, fhandle):
        """Save the best found solution in pickle format

        Parameter
        ---------
        fhandle: file object
            File object with write permission 
        """
        output_dict = dict()
        for i,p in enumerate(self.population):
            if p.is_initialized():
                output_dict[i] = {"objective": p.objective, "variable": p.variable}
        pickle.dump(output_dict, fhandle)
