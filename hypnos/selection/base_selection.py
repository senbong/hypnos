import abc
import functools

import numpy as np



__all__ = [ "coroutine",
            "AbstractSelection",
            "ObjectiveVariableMap" ]



def coroutine(func):
    """Coroutine decorator

    Notes
    -----
    It is used to decorate _evolution internal method.
    """
    @functools.wraps(func)
    def start(*args, **kwargs):
        cr = func(*args, **kwargs)
        cr.next()
        return cr
    return start



class AbstractSelection(object):
    """Interface for all selection operators in hypnos

    Notes
    -----
    All selection operators should implement all the interfaces.  
    """

    __metaclass__ = abc.ABCMeta
    
    @abc.abstractmethod
    def __init__(self, *args, **kwargs):
        pass

    
    @abc.abstractmethod
    def feed(self, variable, objective, index=None):
        """Feed the data into the selection operator

        Parameters
        ----------
        variable: any type except None
            Parameters used to obtain the objective values
            
        objective: list of floats, or numpy array
            Objective vectors

        index: int
            Index value in the population
        """
        pass

    
    @abc.abstractmethod
    def mating_index(self, *args, **kwargs):
        """Get the mating index used in mating selection

        Return
        ------
        list of int
        """
        pass


    @abc.abstractmethod
    def plot(self, *args, **kwargs):
        """Plot the Pareto front in 2D graph
        """
        pass


    @abc.abstractmethod
    def save(self, fhandle, *args, **kwargs):
        """Save the best found solution in pickle format
        """
        pass



class ObjectiveVariableMap(object):
    """Objective and variable mapping
    """

    def __init__(self, minimization):
        self.minimization = minimization
        self.objective = np.finfo(float).max if minimization else np.finfo(float).min
        self.variable = None


    def is_initialized(self):
        """Check whether the subproblem has been initialized

        Return
        ------
        bool
        """
        return self.variable is not None


    def reset(self):
        """Reset the objective-variable mapping to uninitialized state.
        """
        self.objective = np.finfo(float).max if self.minimization else np.finfo(float).min
        self.variable = None
