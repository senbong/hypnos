from __future__ import division

import random
import warnings
import cPickle as pickle

import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial.distance import pdist, squareform

from . import coroutine, AbstractSelection, ObjectiveVariableMap



__all__ = [ "SubProblem",
            "DecompositionSelection",
            "GeometryBasedDecompositionSelection" ]



class SubProblem(ObjectiveVariableMap):
    """SubProblem class used in decomposition-based selection method.

    Parameters
    ----------
    weight: list of float, numpy array
        Weight vectors used to decompose a multi-objective problem.

    minimization: bool
        Maximization or minimization problem.
    """

    def __init__(self, weight, minimization):
        super(SubProblem, self).__init__(minimization)
        self._weight = weight
        

    @property   
    def weight(self):
        return self._weight 
    
    

class DecompositionSelection(AbstractSelection):
    """Decomposition-based multi-objective environmental selection

    Parameters
    ----------
    objective_num: int
        Number of objectives

    interval_num: int
        Number of intervals per each objective

    neighbor_num: int
        Number of neighbor solutions used in mating & environmental selection

    minimization: bool
        Minimization or maximization problem

    n2p_ratio: float
        Ratio between compare/select solution from neighbor to whole population,
        value must be within [0, 1].

    update_limit: int
        Maximum number of solution replacement for each input solution.

    References
    ----------
    .. [1] Zhang, Q., & Li, H. (2007). MOEA/D: A multiobjective evolutionary 
           algorithm based on decomposition. Evolutionary Computation, IEEE 
           Transactions on.
    """
    
    def __init__(self, nobj, interval_num, neighbor_num, minimization=True, 
                n2p_ratio=0.8, update_limit=2):
        self.objective_num = nobj
        self.interval_num  = interval_num
        self.neighbor_num  = neighbor_num
        self.minimization  = minimization

        if not (0.0 <= n2p_ratio <= 1.0):
            raise ValueError("Value of n2p_ratio must be within 0.0-1.0.")

        self.neigh_to_pop  = n2p_ratio
        self.update_limit  = update_limit
        self.population    = list()
        self.ideal_vector  = np.array([np.inf]*nobj) if minimization else np.array([-np.inf]*nobj)

        
        # initialize uniformly spread weight vectors
        weights = list()
        self._uniform_weight(0, interval_num, [0]*nobj, weights)
        for weight in weights:
            self.population.append(SubProblem(weight/interval_num, minimization))
            
        # form the neighborhood map
        self.neighbor_map = dict()
        distance_matrix = squareform(pdist(weights))
        for i in range(len(weights)):
            self.neighbor_map[i] = distance_matrix[i, :].argsort()[:neighbor_num]
        
        # start evolution coroutine
        self._evals_num = 0
        self.evolution  = self._evolution()
        
    
    @property
    def population_size(self):
        return len(self.population)
    
    
    @property
    def evals_num(self):
        return self._evals_num
    
    
    @property
    def generation_num(self):
        return self.evals_num//self.population_size
    
    
    def _uniform_weight(self, level, interval_num, prev_weight, weights):
        """Uniform weight generation using recursive function
        """
        if level < self.objective_num:
            total_weight_element = sum(prev_weight[:level+1])
            if total_weight_element > interval_num:
                return

            if level == self.objective_num - 1:
                prev_weight[level] = interval_num - total_weight_element
                weights.append(np.array(prev_weight))
                return
            else:
                temp = list(prev_weight)
                self._uniform_weight(level+1, interval_num, temp, weights)
                prev_weight[level] += 1
                self._uniform_weight(level, interval_num, prev_weight, weights)
                
    
    def _update_reference(self, objective):
        """Update ideal vector value
        """
        if self.minimization:
            self.ideal_vector = np.min([self.ideal_vector, objective], axis=0)
        else:
            self.ideal_vector = np.max([self.ideal_vector, objective], axis=0)
        
    
    def _update_problem(self, index, variable, objective):
        """Update subproblem
        """
        neighbor_flag = random.random() < self.neigh_to_pop
        iterator = list(self.neighbor_map[index]) if neighbor_flag else range(self.population_size)
        random.shuffle(iterator)
        update_count = 0
        for i in iterator:
            f1 = self._scalarize(self.population[i].objective, self.population[i].weight)
            f2 = self._scalarize(objective, self.population[i].weight)
            
            if f2 < f1:
                self.population[i].objective = objective
                self.population[i].variable  = variable
                update_count += 1
            
            if update_count >= self.update_limit:
                return
            
        
    def _scalarize(self, objective, weight, eps=1e-3):
        """Tchebycheff method to scalarize the multi-objective problem
        """
        weight = np.clip(weight, eps, 1.0)  # fix extreme point problem
        diff = np.abs(objective - self.ideal_vector)
        return np.max(diff * weight)
        
    
    @coroutine
    def _evolution(self):
        """Implement evolutionary process using coroutine
        """
        try:
            for index,subproblem in enumerate(self.population):
                self.cur_index = index
                _, variable, objective = yield
                subproblem.variable = variable
                subproblem.objective = objective
                self._update_reference(np.array(objective))

            while True:
                sequence = range(len(self.population))
                random.shuffle(sequence)
                for index in sequence:
                    self.cur_index = index
                    user_index, variable, objective = yield
                    if user_index is not None:
                        index = user_index
                    self._evals_num += 1
                    self._update_reference(np.array(objective))
                    self._update_problem(index, variable, objective)
        except GeneratorExit:
            print("==== Exit Decomposition Selection ====")
        except:
            print("Unexpected Error!!!")
        
    
    def feed(self, variable, objective, index=None):
        """Feed the data into the selection operator

        Parameters
        ----------
        variable: any type except None
            Parameters used to obtain the objective values
            
        objective: list of floats, or numpy array
            Objective vectors

        index: int
            Index value in the population
        """
        if np.array(objective).size != self.objective_num:
            raise ValueError("Incorrect objective dimension.")

        if variable is None:
            raise ValueError("Variable is None.")
            
        if index and (not isinstance(index, int) or 0 <= index < self.population_size):
            warning_msg = "Ignored input: index({}) must be smaller than population size({}).".format(index, self.population_size)
            warnings.warn(warning_msg, UserWarning)
            return
        
        self.evolution.send((index, variable, objective))
        
        
    def mating_index(self, index=None, parent_size=2):
        """Get the mating index used in mating selection

        Return
        ------
        list of int
        """
        if parent_size > self.neighbor_num:
            raise ValueError("Parent size must not exceed neighborhood size.")

        if index is None:
            index = self.cur_index

        if random.random() < self.neigh_to_pop:
            return random.sample(self.neighbor_map[index], parent_size)
        else:
            return random.sample(range(self.population_size), parent_size)
    
    
    def plot(self, dims=(0, 1), weight_index=0, xlabel=None, ylabel=None, **kwargs):
        """Plot the Pareto front in 2D graph

        Parameters:
            dims: tuple of ints
                Objective indices to be plotted.

            weight_index: int
                Index of the weight vectors which is used to vary the color of 
                the output plot

            xlabel: str
                X-axis label

            ylabel: str
                Y-axis label

            kwargs: keyword arguments
                Additional keyword arguments to customize the plot
        """
        if any(d >= self.objective_num or not isinstance(d, int) for d in dims):
            raise ValueError("Dimensions must be integer values within [0,{})".format(self.objective_num))

        if len(dims) != 2:
            raise ValueError("Size of dims must equal to 2.")
        
        if not isinstance(weight_index, int) or weight_index >= self.objective_num:
            raise ValueError("Weight index must be an integer and less than objective numbers.")

        x_array = [p.objective[dims[0]] for p in self.population if p.is_initialized()]
        y_array = [p.objective[dims[1]] for p in self.population if p.is_initialized()]

        if not kwargs:
            w_array = [p.weight[weight_index] for p in self.population if p.is_initialized()]
            plt.scatter(x_array, y_array, c=w_array)
        else:
            plt.plot(x_array, y_array, **kwargs)

        # display plot
        xlabel = "$f_{}$".format(dims[0]+1) if xlabel is None else xlabel
        ylabel = "$f_{}$".format(dims[1]+1) if ylabel is None else ylabel
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.show()


    def save(self, fhandle):
        """Save the best found solution in pickle format

        Parameter
        ---------
        fhandle: file object
            File object with write permission 
        """
        output_dict = dict()
        for i,p in enumerate(self.population):
            if p.is_initialized():
                output_dict[i] = {"objective": p.objective, "variable": p.variable}
        pickle.dump(output_dict, fhandle)



class GeometryBasedDecompositionSelection(DecompositionSelection):
    """Decomposition-based multi-objective environmental selection with 
    diversity assessment

    Parameters
    ----------
    objective_num: int
        Number of objectives

    interval_num: int
        Number of intervals per each objective

    neighbor_num: int
        Number of neighbor solutions used in mating & environmental selection

    minimization: bool
        Minimization or maximization problem

    n2p_ratio: float
        Ratio between compare/select solution from neighbor to whole population,
        value must be within [0, 1].

    mrdl_threshold: float
        Maximum allowable relative diversity loss.

    References
    ----------
    .. [1] Zhang, Q., & Li, H. (2007). MOEA/D: A Multiobjective Evolutionary 
           Algorithm based on Decomposition. Evolutionary Computation, IEEE 
           Transactions on.

    .. [2] Gee, S., Tan, K., Shim, V., & Pal, N. (2014). Online Diversity 
           Assessment in Evolutionary Multiobjective Optimization: A Geometrical 
           Perspective.
    """

    def __init__(self, nobj, interval_num, neighbor_num, minimization=True, n2p_ratio=0.8,
                 mrdl_threshold=20):
        super(GeometryBasedDecompositionSelection, self).__init__(nobj, interval_num, 
                    neighbor_num, minimization, n2p_ratio)
        if mrdl_threshold < 0:
            raise ValueError("Value of mrdl_threshold must be positive")

        if mrdl_threshold < 10:
            warning_msg = "Too small mrdl_threshold value may cause very slow convergence."
            warnings.warn(warning_msg, UserWarning)
        self.mrdl_threshold_squared = mrdl_threshold**2
        

    def _dominate(self, objective1, objective2):
        obj1, obj2 = np.array(objective1), np.array(objective2)
        if self.minimization:
            return np.all(obj1 <= obj2) and np.any(obj1 != obj2)
        else:
            return np.all(obj1 >= obj2) and np.any(obj1 != obj2)
        

    def _update_problem(self, variable, objective):
        """Update subproblem
        """
        is_better, min_dist, min_dist_index = False, np.finfo(float).max, 0
        iterator = range(self.population_size)
        random.shuffle(iterator)
        for i in iterator:
            f1 = self._scalarize(self.population[i].objective, self.population[i].weight)
            f2 = self._scalarize(objective, self.population[i].weight)
            
            if f2 < f1:
                is_better = True
                
                euclidean_dist = np.linalg.norm(np.array(self.population[i].objective - np.array(objective)))
                if euclidean_dist < min_dist:
                    min_dist = euclidean_dist
                    min_dist_index = i
                
                if len(self.reference_from) == 0:
                    continue
                else:
                    for ref_from, ref_to, dist in zip(self.reference_from, self.reference_to, self.dist_from_to):
                        side_a = np.linalg.norm(ref_from - np.array(self.population[i].objective))
                        side_b = np.linalg.norm(ref_to - np.array(self.population[i].objective))
                        s = 0.5*(side_a + side_b + dist)
                        area_first = s*(s - side_a)*(s - side_b)*(s - dist)     # omit sqrt to save computational time
                        
                        side_a = np.linalg.norm(ref_from - np.array(objective))
                        side_b = np.linalg.norm(ref_to - np.array(objective))
                        s = 0.5*(side_a + side_b + dist)
                        area_second = s*(s - side_a)*(s - side_b)*(s - dist)    # omit sqrt to save computational time
                        
                        if (area_second == 0) or (area_first > self.mrdl_threshold_squared*area_second):
                            is_better = False

        if is_better:

            if self._dominate(objective, self.population[min_dist_index].objective):
                # store references
                self.reference_from.append(np.array(self.population[min_dist_index].objective))
                self.reference_to.append(np.array(objective))
                self.dist_from_to.append(np.linalg.norm(self.reference_from[-1] - self.reference_to[-1]))
            
            # replace solution
            self.population[min_dist_index].objective = objective
            self.population[min_dist_index].variable  = variable
                
            
    @coroutine
    def _evolution(self):
        """Implement evolutionary process using coroutine
        """
        try:
            for index,subproblem in enumerate(self.population):
                self.cur_index = index
                _, variable, objective = yield
                subproblem.variable = variable
                subproblem.objective = objective
                self._update_reference(np.array(objective))

            while True:
                sequence = range(len(self.population))
                random.shuffle(sequence)
                self.reference_from = list()
                self.reference_to   = list()
                self.dist_from_to   = list()
                for index in sequence:
                    self.cur_index = index
                    user_index, variable, objective = yield
                    if user_index is not None:
                        index = user_index
                    self._evals_num += 1
                    self._update_reference(np.array(objective))
                    self._update_problem(variable, objective)
        except GeneratorExit:
            print("==== Exit Geometry-based Decomposition Selection ====")
        except:
            print("Unexpected Error!!!")
