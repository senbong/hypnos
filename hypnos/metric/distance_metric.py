from __future__ import division

import numpy as np
from scipy.spatial.distance import cdist

from . import BaseMetricSuite
from ..util import validation



__all__ = [ "DistanceMetricSuite" ]



class DistanceMetricSuite(BaseMetricSuite):
    """Distance related performance metrics which include inverted generational
    distance (IGD), generational distance (GD) and average Hausdorff distance.

    Parameters
    ----------
    dmetric: 'euclidean' or 'minkowski'
        The distance metric used to calculate the optimal solutions and output
        solutions. This class relies on the scipy's cdist method therefore all 
        the distance metric available in the cdist method can be used here. 
        Generally, 'euclidean' or 'minkowski' are mainly used by the community.
        
    p: int
        The p-norm used in 'minkowski' distance metric.

    pmetric: iterable
        The performance metric(s) which will be stored in history record.

    record: bool
        Whether to store the performance metric(s) history.

    Attributes
    ----------
    inverted_generational_distance: float
        Inverted generational distance (IGD) metric between optimal solution
        set and approximate solution set.

    generational_distance: float
        Generational distance (GD) metric between optimal solution set and
        approximate solution set.

    average_hausdorff_distance: float
        Average Hausdorff distance metric between optimal solution set and
        approximate solution set.
    """

    def __init__(self, dmetric="euclidean", p=2,
            pmetrics=None, record=True):
        super(DistanceMetricSuite, self).__init__(record)
        self.p = p
        self.dmetric = dmetric
        self.distance_matrix = None
        self.pmetrics = list()
        pmetrics = ["inverted_generational_distance"] if pmetrics is None else pmetrics
        for pm in pmetrics:
            try:
                getattr(self, pm)
            except validation.NotUpdatedError:
                self.pmetrics.append(pm)


    def update(self, optimal_solution, approximate_solution, marker="default"):
        """Update the performance metric value

        Parameters
        ----------
        optimal_solution: 2-d numpy array
            Matrix with size kxm where k is the number of optimal points and m 
            is the number of objectives

        output_solution: 2-d numpy array
            Matrix with size nxm where n is the number of output solutions and 
            m is the number of objectives
        """
        self.distance_matrix = cdist(optimal_solution, approximate_solution,
                metric=self.dmetric, p=self.p)
        for pm in self.pmetrics:
            self.history[pm][marker].append(getattr(self, pm))


    def _check_update(self):
        if self.distance_matrix is None:
            raise validation.NotUpdatedError


    @property
    def generational_distance(self):
        self._check_update()
        return np.mean(np.min(self.distance_matrix, axis=0))


    @property
    def inverted_generational_distance(self):
        self._check_update()
        return np.mean(np.min(self.distance_matrix, axis=1))
    

    @property
    def average_hausdorff_distance(self):
        self._check_update()
        distance_matrix_p = np.power(self.distance_matrix, self.p)
        d_xy_p = np.sum(np.min(distance_matrix_p, axis=0))**(1.0/self.p)
        d_yx_p = np.sum(np.min(distance_matrix_p, axis=1))**(1.0/self.p)
        k1 = 1/np.sqrt(distance_matrix_p.shape[1])
        k2 = 1/np.sqrt(distance_matrix_p.shape[0])
        return max(k1*d_xy_p, k2*d_yx_p)
