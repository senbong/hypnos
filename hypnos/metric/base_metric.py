import abc
import sys
from collections import defaultdict

import numpy as np
import matplotlib.pyplot as plt



__all__ = [ "AbstractMetricSuite",
            "BaseMetricSuite" ]



class AbstractMetricSuite(object):
    """Interface for performance metric in hypnos
    """

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def update(self, optimal_solution, output_solution, *args, **kwargs):
        """Update the performance metric value

        Parameters
        ----------
        optimal_solution: 2-d numpy array
            Matrix with size kxm where k is the number of optimal points and m 
            is the number of objectives

        output_solution: 2-d numpy array
            Matrix with size nxm where n is the number of output solutions and 
            m is the number of objectives
        """
        pass


class BaseMetricSuite(AbstractMetricSuite):
    """Base class for performance metric
    """

    def __init__(self, record=True):
        self.record = record
        self.history = defaultdict(lambda :defaultdict(list)) if record else None


    def plot(self):
        if len(self.history):
            f, axarr = plt.subplots(len(self.history), sharex=True)
            for i,pm in enumerate(self.history):
                axis_handle = axarr[i] if type(axarr) is np.ndarray else axarr
                for m in self.history[pm]:
                    generations = [1+k for k in range(len(self.history[pm][m]))]
                    axis_handle.plot(generations, self.history[pm][m], label=m)
                    axis_handle.set_title(pm)

                if i == len(self.history) - 1:
                    axis_handle.legend(loc="upper center", bbox_to_anchor=(0, -0.15, 1, -0.15),
                            ncol=2, mode="expand", borderaxespad=0)
        else:
            sys.stderr.write("Empty performance metric history.\n")
