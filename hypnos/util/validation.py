
class NotUpdatedError(ValueError, AttributeError):
    """Exception class to raise if metric class has not been updated
    """
    pass


class PathNotSetError(ValueError, AttributeError):
    """Exception class to raise if solution path has not been set
    """
    pass

