import abc
import inspect
import random

from ..selection import AbstractSelection


__all__ = [ "AbstractVariator",
            "AbstractBounder",
            "OneOffspringVariator",
            "TwoOffspringVariator" ]



class AbstractVariator(object):
    """Interface for recombination operators in hypnos
    """

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def generate(self, *args, **kwargs):
        """Function to generate new decision variable input(s)
        """
        pass



class AbstractBounder(object):
    """Interface for solution repairment operator
    """

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def __call__(self, x):
        """Function to repair input decision variable
        """
        pass



class OneOffspringVariator(AbstractVariator):
    """Variator to generate one offspring solution
    """
    
    def __init__(self, selector, xover=None, mutator=None, method="first"):

        if not isinstance(selector, AbstractSelection):
            raise ValueError("selector must be an instance of AbstractSelection class.")
        
        if xover is None and mutator is None:
            raise ValueError("At least a xover or mutator operator must be provided.")
        
        if xover is not None and not isinstance(xover, AbstractVariator):
            raise ValueError("xover must be an instance of AbstractVariator class.")

        if mutator is not None and not isinstance(mutator, AbstractVariator):
            raise ValueError("mutator must be an instance of AbstractVariator class.")

        methods = {"first":lambda :0, "second":lambda :1, "rand":lambda :random.randinit(0, 1)}
        
        if method not in methods:
            raise ValueError("Value of method must be in the set {}".format(methods.keys()))

        self.selector = selector
        self.xover = xover
        self.mutator = mutator
        self.method = methods[method]


    def generate(self):

        if self.xover is not None:
            parent_num = len(inspect.getargspec(self.xover.generate).args) - 1
            parent_indices = self.selector.mating_index(parent_size=parent_num)
            parents = [self.selector.population[p].variable for p in parent_indices]
            child = self.xover.generate(*parents)[self.method()]

        if self.mutator is not None:
            parent_index = self.selector.mating_index(parent_size=1)[0]
            child = child if 'child' in locals() else self.selector.population[parent_index].variable
            child = self.mutator.generate(child)
        return child



class TwoOffspringVariator(AbstractVariator):
    """Variator to generate two offspring solution
    """
    
    def __init__(self, selector, xover, mutator=None):

        if not isinstance(selector, AbstractSelection):
            raise ValueError("selector must be an instance of AbstractSelection class.")
        
        if not isinstance(xover, AbstractVariator):
            raise ValueError("xover must be an instance of AbstractVariator class.")

        if mutator is not None and not isinstance(mutator, AbstractVariator):
            raise ValueError("mutator must be an instance of AbstractVariator class.")

        self.selector = selector
        self.xover = xover
        self.mutator = mutator


    def generate(self):

        parent_num = len(inspect.getargspec(self.xover.generate).args) - 1
        parent_indices = self.selector.mating_index(parent_size=parent_num)
        parents = [self.selector.population[p].variable for p in parent_indices]
        child1, child2 = self.xover.generate(*parents)[self.method()]

        if self.mutator is not None:
            child1 = self.mutator.generate(child1)
            child2 = self.mutator.generate(child2)
        return child1, child2
