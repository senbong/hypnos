from __future__ import division

import warnings

import numpy as np

from . import AbstractVariator, AbstractBounder



__all__ = [ "BoxBounder",
            "DifferentialEvolution",
            "SimulatedBinaryXover",
            "BlendAlphaXover",
            "LaplaceXover",
            "GaussianMutation",
            "PolynomialMutation" ]   



class BoxBounder(AbstractBounder):
    """Bounder class to repair the solution which falls into infeasible region.

    Parameters
    ----------
    upper_bound: int, list, or numpy array
        Upper bound of the feasible decision space

    lower_bound: int, list, or numpy array
        Lower bound of the feasible decision space

    method: "clip", "mirror", or "cycle"
        Method used to repair the solution 
    """

    def __init__(self, upper_bound, lower_bound, method="clip"):

        if not np.all(upper_bound > lower_bound):
            raise ValueError("All upper_bound value(s) must be greater than lower_bound value(s).")

        self.upper_bound = np.array(upper_bound)
        self.lower_bound = np.array(lower_bound)
        self.interval = self.upper_bound - self.lower_bound
        self.bounder = { "clip": lambda x: np.clip(x, lower_bound, upper_bound),
                "mirror": lambda x:self.upper_bound - (x - self.lower_bound) % self.interval,
                "cycle": lambda x:(x - self.lower_bound) % self.interval + self.lower_bound}

        if method not in self.bounder:
            raise ValueError("Value of method must be within {clip, mirror, cycle}.")
        self.method = method


    def __call__(self, x):
        return self.bounder[self.method](x)



class DifferentialEvolution(AbstractVariator):
    """Differential evolution (DE)

    Parameters
    ----------
    bounder: extended class object of AbstractBounder class
        Bounder object used to repair the generated solution

    scaling_factor: float
        DE parameter used to scale the difference vector

    xover_rate: float
        Crossover rate

    References
    ----------
    .. [1] Storn, R., & Price, K. (1997). Differential evolution-a simple and 
           efficient heuristic for global optimization over continuous spaces. 
           Journal of global optimization.
    """

    def __init__(self, bounder, scaling_factor=0.5, xover_rate=1.0):

        if not hasattr(bounder, '__call__'):
            raise AttributeError("Input bounder is not callable.")

        if not (0.0 <= xover_rate <= 1.0):
            raise ValueError("Value of xover_rate must be within 0.0-1.0.")

        if not (0.0 <= scaling_factor <= 2.0):
            warning_msg = "Value of scaling_factor falls outside 0.0-2.0."
            warnings.warn(warning_msg, UserWarning)
        
        self.bounder = bounder
        self.phi  = scaling_factor
        self.xover_rate = xover_rate
        

    def generate(self, x1, x2, x3):

        v = np.array(x1) + self.phi*(np.array(x2) - np.array(x3))
        xover_mask = np.random.random(len(x1)) <= self.xover_rate
        child1 = self.bounder(v*xover_mask + x1*np.invert(xover_mask))
        child2 = self.bounder(v*np.invert(xover_mask) + x1*xover_mask)
        return child1, child2
        


class SimulatedBinaryXover(AbstractVariator):
    """Simulated Binary Crossover (SBX)

    Parameters
    ----------
    bounder: extended class object of AbstractBounder class
        Bounder object used to repair the generated solution

    eta_c: float
        A non-negative real number and large value of eta_c gives a higher 
        probability of creating near-parent solutions

    References
    ----------
    .. [1] Deb, K., & Agrawal, R. B. (1994). Simulated binary crossover for 
           continuous search space. Complex Systems.
    """

    def __init__(self, bounder, eta_c=10):
        
        if not hasattr(bounder, '__call__'):
            raise AttributeError("Input bounder is not callable.")

        if not hasattr(bounder, 'lower_bound') or not hasattr(bounder, 'upper_bound'):
            raise AttributeError("Missing lower_bound and upper_bound attribute in bounder.")

        if eta_c < 0:
            raise ValueError("Value of eta_c must be non-negative.")

        self.eta_c = eta_c
        self.bounder = bounder


    def generate(self, x1, x2):
        
        upper = np.max([np.array(x1), np.array(x2)], axis=0)
        lower = np.min([np.array(x1), np.array(x2)], axis=0)
        interval = np.clip(upper - lower, np.finfo(float).eps,
                self.bounder.upper_bound - self.bounder.lower_bound)
        beta = 1.0 + 2 * np.min([lower - self.bounder.lower_bound, self.bounder.upper_bound - upper], axis=0)/(interval) 
        alpha = 2.0 - np.power(beta, -(self.eta_c + 1.0))
        u = np.random.random(len(x1))
        vfunc = np.vectorize(self._beta_q_helper)
        beta_q = vfunc(u, alpha)
        c1 = 0.5*((lower + upper) - beta_q*(upper - lower))
        c1 = self.bounder(c1)
        c2 = 0.5*((lower + upper) + beta_q*(upper - lower))
        c2 = self.bounder(c2)
        xover_mask = np.random.random(len(x1)) > 0.5
        child1 = c1*xover_mask + c2*np.invert(xover_mask)
        child2 = c1*np.invert(xover_mask) + c2*xover_mask
        return child1, child2


    def _beta_q_helper(self, u, alpha):

        if u <= (1.0/alpha):
            return (u*alpha)**(1.0/(self.eta_c + 1.0))
        else:
            return (1.0/(2.0 - u*alpha))**(1.0/(self.eta_c + 1.0))



class BlendAlphaXover(AbstractVariator):
    """Blend-alpha Crossover (BLX-alpha)

    Parameters
    ----------
    bounder: extended class object of AbstractBounder class
        Bounder object used to repair the generated solution

    alpha: float
        Blending rate falls within 0.0-1.0

    xover_rate: float
        Crossover rate 

    References
    ----------
    .. [1] Eshelman, L. J., & Schaffer, J. D. (1992). Real--Coded Genetic 
           Algorithms and Interval-Schemata.
    """

    def __init__(self, bounder, alpha=0.5, xover_rate=1.0):

        if not hasattr(bounder, '__call__'):
            raise AttributeError("Input bounder is not callable.")

        if not (0.0 <= xover_rate <= 1.0):
            raise ValueError("Value of xover_rate must be within 0.0-1.0.")

        self.bounder = bounder
        self.alpha = alpha
        self.xover_rate = xover_rate


    def generate(self, x1, x2):

        upper = np.max([np.array(x1), np.array(x2)], axis=0)
        lower = np.min([np.array(x1), np.array(x2)], axis=0)
        delta = self.alpha * (upper - lower)
        c1 = lower - delta + np.random.random(len(x1))*(upper - lower + 2*delta)
        c2 = lower - delta + np.random.random(len(x2))*(upper - lower + 2*delta)
        xover_mask = np.random.random(len(x1)) <= self.xover_rate
        child1 = self.bounder(c1*xover_mask + x1*np.invert(xover_mask))
        child2 = self.bounder(c2*xover_mask + x2*np.invert(xover_mask))
        return child1, child2



class LaplaceXover(AbstractVariator):
    """Laplace Crossover (LX)

    Parameters
    ----------
    bounder: extended class object of AbstractBounder class
        Bounder object used to repair the generated solution

    loc: float
        Mean of the Laplace distribution

    scale: float
        Scale of the Laplace distribution

    xover_rate: float
        Crossover rate 

    References
    ----------
    .. [1] Deep, K., & Thakur, M. (2007). A new crossover operator for real 
           coded genetic algorithms. Applied Mathematics and Computation. 
    """

    def __init__(self, bounder, loc=0, scale=0.5, xover_rate=1.0):

        if not hasattr(bounder, '__call__'):
            raise AttributeError("Input bounder is not callable.")

        if scale < 0:
            raise ValueError("Value of scale must be non-negative.")

        if not (0.0 <= xover_rate <= 1.0):
            raise ValueError("Value of xover_rate must be within 0.0-1.0.")

        self.bounder = bounder
        self.loc = loc
        self.scale = scale
        self.xover_rate = xover_rate


    def generate(self, x1, x2):

        beta = np.random.laplace(self.loc, self.scale, len(x1))
        c1 = np.array(x1) + beta*np.abs(np.array(x1) - np.array(x2))
        c2 = np.array(x2) + beta*np.abs(np.array(x1) - np.array(x2))
        xover_mask = np.random.random(len(x1)) <= self.xover_rate
        child1 = self.bounder(c1*xover_mask + x1*np.invert(xover_mask))
        child2 = self.bounder(c2*xover_mask + x2*np.invert(xover_mask))
        return child1, child2



class GaussianMutation(AbstractVariator):

    def __init__(self, bounder, mean=0.0, stdev=1.0, mutation_rate=0.1):
        
        if not hasattr(bounder, '__call__'):
            raise AttributeError("Input bounder is not callable.")

        if stdev < 0:
            raise ValueError("Value of stdev must be non-negative.")

        if not (0 <= mutation_rate <= 1.0):
            raise ValueError("Value of mutation_rate must be within 0.0-1.0.")

        self.bounder = bounder
        self.mean = mean
        self.stdev = stdev
        self.mutation_rate = mutation_rate


    def generate(self, x):

        mutation_mask = np.random.random(len(x)) <= self.mutation_rate
        mutated_x = np.array(x) + np.random.normal(self.mean, self.stdev, len(x))
        return self.bounder(np.invert(mutation_mask)*np.array(x) + mutation_mask*mutated_x)
        


class PolynomialMutation(AbstractVariator):
    """Polynomial Mutation operator

    Parameters
    ----------
    bounder: extended class object of AbstractBounder class
        Bounder object used to repair the generated solution

    eta_m: float
        Distribution index

    References
    ----------
    .. [1] Ripon, K. S. N., Kwong, S., & Man, K. F. (2007). A real-coding 
           jumping gene genetic algorithm (RJGGA) for multiobjective 
           optimization. Information Sciences.
    """

    def __init__(self, bounder, eta_m=10, mutation_rate=0.1):

        if not hasattr(bounder, '__call__'):
            raise AttributeError("Input bounder is not callable.")

        if eta_m < 0:
            raise ValueError("Value of eta_m must be non-negative.")
        
        self.bounder = bounder
        self.eta_m = eta_m
        self.mutation_rate = mutation_rate


    def _polynomial_helper(self, delta1, delta2, mutation_power, eta_m):

        rand = np.random.random()
        if rand < 0.5:
            xy = 1.0 - delta1
            val = 2.0*rand + (1.0 - 2.0*rand)*xy**(eta_m + 1)
            delta_q = val**mutation_power - 1.0
        else:
            xy = 1.0 - delta2
            val = 2.0*(1.0 - rand) + 2.0*(rand - 0.5)*xy**(eta_m + 1)
            delta_q = val**mutation_power
        return delta_q


    def generate(self, x):

        interval = self.bounder.upper_bound - self.bounder.lower_bound
        delta1 = (x - self.bounder.lower_bound)/interval
        delta2 = (self.bounder.upper_bound - x)/interval
        mutation_power = 1.0 / (self.eta_m + 1.0)
        vfunc = np.vectorize(self._polynomial_helper)
        delta_q = vfunc(delta1, delta2, mutation_power, self.eta_m)
        mutated_x = x + delta_q*interval
        mutation_mask = np.random.random(len(x)) <= self.mutation_rate
        return self.bounder(np.invert(mutation_mask)*np.array(x) + mutation_mask*mutated_x)
