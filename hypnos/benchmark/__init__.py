from .base_benchmark import *
from .zdt_benchmark import *
from .wfg_benchmark import *
from .cec09_benchmark import *
from .fda_benchmark import *
#from .gta_benchmark import *
