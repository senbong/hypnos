from __future__ import division

import abc

import numpy as np

from . import BaseBenchmark, StaticMOPMixin
from . import check_xval, increment_fevals, accumulate_time



__all__ = [ "UF1",
            "UF2",
            "UF3",
            "UF4",
            "UF5",
            "UF6",
            "UF7",
            "UF8",
            "UF9",
            "UF10",
            "CEC09BaseBenchmark" ]



class CEC09BaseBenchmark(BaseBenchmark, StaticMOPMixin):
    """Base class for CEC-09 benchmark test problem"""
    objective_num = 2
    variable_num = 30
    lower_bound = np.array([0.0] + 29*[-1.0])
    upper_bound = np.array(30*[1.0])

    @classmethod
    def solution_generate(cls):
        interval = cls.upper_bound[0] - cls.lower_bound[0]
        x1 = interval*np.random.random() + cls.lower_bound[0]
        xj = [np.sin(6*np.pi*x1 + j*np.pi/cls.variable_num)
                for j in range(2, cls.variable_num + 1)]
        return np.array([x1] + xj)
    
    
    
class UF1(CEC09BaseBenchmark):
    """UF1 Multi-objective optimization benchmark problem"""
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        f1, f2 = x[0], 1 - np.sqrt(x[0])
        beta1, beta2, J1, J2 = 0, 0, 0, 0
        for i,_x in enumerate(x[1:]):
            i += 2
            temp = (_x - np.sin(6*np.pi*x[0] + i*np.pi/cls.variable_num))**2
            if i % 2:
                beta1 += temp
                J1 += 1
            else:
                beta2 += temp
                J2 += 1
        f1 += (2.0/J1) * beta1
        f2 += (2.0/J2) * beta2
        return np.array([f1, f2])
    
    
    
class UF2(CEC09BaseBenchmark):
    """UF2 Multi-objective optimization benchmark problem"""
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        f1, f2 = x[0], 1 - np.sqrt(x[0])
        beta1, beta2, J1, J2 = 0, 0, 0, 0
        for i,_x in enumerate(x[1:]):
            temp = (0.3*x[0]**2*np.cos(24*np.pi*x[0] + 4*i*np.pi/cls.variable_num) + 0.6*x[0])
            if i % 2:
                beta1 += (_x - temp*np.cos(6*np.pi*x[0] + i*np.pi/cls.variable_num))**2
                J1 += 1
            else:
                beta2 += (_x - temp*np.sin(6*np.pi*x[0] + i*np.pi/cls.variable_num))**2
                J2 += 1
        f1 += (2.0/J1) * beta1
        f2 += (2.0/J2) * beta2
        return np.array([f1, f2])
    
    
    @classmethod
    def solution_generate(cls):
        interval = cls.upper_bound[0] - cls.lower_bound[0]
        x1 = interval*np.random.random() + cls.lower_bound[0]
        h1 = lambda j,x:0.3*x**2*np.cos(24*np.pi*x + 4*j*np.pi/cls.variable_num) + 0.6*x
        h2 = lambda j,x,f:f(6*np.pi*x + j*np.pi/cls.variable_num)
        xj = list()
        for j in range(cls.variable_num - 1):
            if j % 2:
                xj.append(h1(j, x1)*h2(j, x1, np.cos))
            else:
                xj.append(h1(j, x1)*h2(j, x1, np.sin))
        return np.array([x1] + xj)


    
class UF3(CEC09BaseBenchmark):
    """UF3 Multi-objective optimization benchmark problem"""
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        f1, f2 = x[0], 1 - np.sqrt(x[0])
        beta1, beta2, alpha1, alpha2 = 0, 0, 1, 1
        J1, J2 = 0, 0
        for i,_x in enumerate(x[1:]):
            i += 2
            _y = (_x - x[0]**(0.5*(1.0 + 3*(i-2.0)/(cls.variable_num-2.0))))
            temp = np.cos(20*_y*np.pi/np.sqrt(i))
            if i % 2:
                beta1 += _y**2
                alpha1 *= temp
                J1 += 1
            else:
                beta2 += _y**2
                alpha2 *= temp
                J2 += 1
        f1 += (2.0/J1) * (4*beta1 - 2*alpha1 + 2)
        f2 += (2.0/J2) * (4*beta2 - 2*alpha2 + 2)
        return np.array([f1, f2])
    
    
    @classmethod
    def solution_generate(cls):
        interval = cls.upper_bound[0] - cls.lower_bound[0]
        x1 = interval*np.random.random() + cls.lower_bound[0]
        h = lambda j,x:x**(0.5*(1.0 + 3.0*(j-2)/(cls.variable_num-2)))
        xj = [h(j,x1) for j in range(2, cls.variable_num + 1)]
        return np.array([x1] + xj)


    
class UF4(CEC09BaseBenchmark):
    """UF4 Multi-objective optimization benchmark problem"""
    lower_bound = np.array([0.0] + 29*[-2.0])
    upper_bound = np.array([1.0] + 29*[2.0])
    
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        f1, f2 = x[0], 1 - x[0]**2
        h = lambda x: np.abs(x)/(1.0 + np.exp(2*np.abs(x)))
        beta1, beta2, J1, J2 = 0, 0, 0, 0
        for i,_x in enumerate(x[1:]):
            i += 2
            _y = _x - np.sin(6*np.pi*x[0] + i*np.pi/cls.variable_num)
            if i % 2:
                beta1 += h(_y)
                J1 += 1
            else:
                beta2 += h(_y)
                J2 += 1
        f1 += (2.0/J1) * beta1
        f2 += (2.0/J2) * beta2
        return [f1, f2]
    


class UF5(CEC09BaseBenchmark):
    """UF5 Multi-objective optimization benchmark problem"""
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        f1, f2, epsilon, N = x[0], 1 - x[0], 0.1, 10
        h = lambda x: 2*x**2 - np.cos(4*np.pi*x) + 1.0
        beta1, beta2, J1, J2 = 0, 0, 0, 0
        for i,_x in enumerate(x[1:]):
            i += 2
            _y = _x - np.sin(6*np.pi*x[0] + i*np.pi/cls.variable_num)
            if i % 2:
                beta1 += h(_y)
                J1 += 1
            else:
                beta2 += h(_y)
                J2 += 1
        f1 += (1.0/(2*N) + epsilon)*np.fabs(np.sin(2*N*np.pi*x[0])) + (2.0/J1)*beta1
        f2 += (1.0/(2*N) + epsilon)*np.fabs(np.sin(2*N*np.pi*x[0])) + (2.0/J2)*beta2
        return np.array([f1, f2])
    
    
    
class UF6(CEC09BaseBenchmark):
    """UF6 Multi-objective optimization benchmark problem"""
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        f1, f2, epsilon, N = x[0], 1 - x[0], 0.1, 2
        beta1, beta2, alpha1, alpha2 = 0, 0, 1, 1
        J1, J2 = 0, 0
        for i,_x in enumerate(x[1:]):
            i += 2
            _y = _x - np.sin(6*np.pi*x[0] + i*np.pi/cls.variable_num)
            temp = np.cos(20*_y*np.pi/np.sqrt(i) + 2)
            if i % 2:
                beta1 += _y**2
                alpha1 *= temp
                J1 += 1
            else:
                beta2 += _y**2
                alpha1 *= temp
                J2 += 1
        f1 += max(0, 2*(1.0/(2*N) + epsilon)*np.sin(2*N*np.pi*x[0])) + \
                (2.0/J1)*(4*beta1 - 2*alpha1 + 2)
        f2 += max(0, 2*(1.0/(2*N) + epsilon)*np.sin(2*N*np.pi*x[0])) + \
                (2.0/J2)*(4*beta2 - 2*alpha2 + 2)
        return np.array([f1, f2])
    
    

class UF7(CEC09BaseBenchmark):
    """UF7 Multi-objective optimization benchmark problem"""
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        f1, f2 = x[0]**0.2, 1 - x[0]**0.2
        beta1, beta2, J1, J2 = 0, 0, 0, 0
        for i,_x in enumerate(x[1:]):
            i += 2
            _y = _x - np.sin(6*np.pi*x[0] + i*np.pi/cls.variable_num)
            if i % 2:
                beta1 += _y**2
                J1 += 1
            else:
                beta2 += _y**2
                J2 += 1
        f1 += (2.0/J1) * beta1
        f2 += (2.0/J2) * beta2
        return np.array([f1, f2])
    
    
    @classmethod
    def solution_generate(cls):
        interval = cls.upper_bound[0] - cls.lower_bound[0]
        x1 = (interval*np.random.random() + cls.lower_bound[0])**5
        xj = [np.sin(6*np.pi*x1 + j*np.pi/cls.variable_num)
                for j in range(2, cls.variable_num + 1)]
        return np.array([x1] + xj)

    

class UF8(CEC09BaseBenchmark):
    """UF8 Multi-objective optimization benchmark problem"""
    objective_num = 3
    lower_bound = np.array(2*[0.0] + 28*[-2.0])
    upper_bound = np.array(2*[1.0] + 28*[2.0])

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        f1 = np.cos(0.5*x[0]*np.pi)*np.cos(0.5*x[1]*np.pi)
        f2 = np.cos(0.5*x[0]*np.pi)*np.sin(0.5*x[1]*np.pi)
        f3 = np.sin(0.5*x[0]*np.pi)
        beta1, beta2, beta3 = 0, 0, 0
        J1, J2, J3 = 0, 0, 0
        for i,_x in enumerate(x[2:]):
            i += 3
            _y = (_x - 2*x[1]*np.sin(2*np.pi*x[0] + (i*np.pi)/cls.variable_num))**2
            if (i - 1) % 3 == 0:
                beta1 += _y
                J1 += 1
            if (i - 2) % 3 == 0:
                beta2 += _y
                J2 += 1
            if i % 3 == 0:
                beta3 += _y
                J3 += 1
        f1 += (2.0/J1)*beta1
        f2 += (2.0/J2)*beta2
        f3 += (2.0/J3)*beta3
        return np.array([f1, f2, f3])
    
    
    @classmethod
    def solution_generate(cls):
        interval = cls.upper_bound[:2] - cls.lower_bound[:2]
        xI = interval*np.random.random(2) + cls.lower_bound[:2]
        h = lambda j,x1,x2:2*x2*np.sin(2*np.pi*x1 + j*np.pi/cls.variable_num)
        xj = [h(j,xI[0],xI[1]) for j in range(3, cls.variable_num + 1)]
        return np.array(list(xI) + xj)
    


class UF9(CEC09BaseBenchmark):
    """UF9 Multi-objective optimization benchmark problem"""
    objective_num = 3
    lower_bound = np.array(2*[0.0] + 28*[-2.0])
    upper_bound = np.array(2*[1.0] + 28*[2.0])
    
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        epsilon = 0.1
        f1 = 0.5*(max(0, (1 + epsilon)*(1 - 4*(2*x[0] - 1)**2)) + 2*x[0])*x[1]
        f2 = 0.5*(max(0, (1 + epsilon)*(1 - 4*(2*x[0] - 1)**2)) - 2*x[0] + 2)*x[1]
        f3 = 1 - x[1]
        beta1, beta2, beta3 = 0, 0, 0
        J1, J2, J3 = 0, 0, 0
        for i,_x in enumerate(x[2:]):
            i += 3
            _y = (_x - 2*x[1]*np.sin(2*np.pi*x[0] + (i*np.pi)/cls.variable_num))**2
            if (i - 1) % 3 == 0:
                beta1 += _y
                J1 += 1
            if (i - 2) % 3 == 0:
                beta2 += _y
                J2 += 1
            if i % 3 == 0:
                beta3 += _y
                J3 += 1
        f1 += (2.0/J1)*beta1
        f2 += (2.0/J2)*beta2
        f3 += (2.0/J3)*beta3
        return np.array([f1, f2, f3])
    
    
    @classmethod
    def solution_generate(cls):
        interval = cls.upper_bound[:2] - cls.lower_bound[:2]
        x1 = interval[0]*np.random.random() + cls.lower_bound[0]
        while not ((0 <= x1 <= 0.25) or (0.75 <= x1 <= 1.0)):
            x1 = interval[0]*np.random.random() + cls.lower_bound[0]
        x2 = interval[1]*np.random.random() + cls.lower_bound[1]
        h = lambda j,x1,x2:2*x2*np.sin(2*np.pi*x1 + j*np.pi/cls.variable_num)
        xj = [h(j,x1, x2) for j in range(3, cls.variable_num+1)]
        return np.array([x1, x2] + xj)
        
    

class UF10(CEC09BaseBenchmark):
    """UF10 Multi-objective optimization benchmark problem"""
    objective_num = 3
    lower_bound = np.array(2*[0.0] + 28*[-2.0])
    upper_bound = np.array(2*[1.0] + 28*[2.0])
    
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        f1 = np.cos(0.5*x[0]*np.pi)*np.cos(0.5*x[1]*np.pi)
        f2 = np.cos(0.5*x[0]*np.pi)*np.sin(0.5*x[1]*np.pi)
        f3 = np.sin(0.5*x[0]*np.pi)
        beta1, beta2, beta3 = 0, 0, 0
        J1, J2, J3 = 0, 0, 0
        for i,_x in enumerate(x[2:]):
            i += 3
            _y = (_x - 2*x[1]*np.sin(2*np.pi*x[0] + (i*np.pi)/cls.variable_num))
            temp = 4*_y**2 - np.cos(8*np.pi*_y) + 1
            if (i - 1) % 3 == 0:
                beta1 += temp
                J1 += 1
            if (i - 2) % 3 == 0:
                beta2 += temp
                J2 += 1
            if i % 3 == 0:
                beta3 += temp
                J3 += 1
        f1 += (2.0/J1)*beta1
        f2 += (2.0/J2)*beta2
        f3 += (2.0/J3)*beta3
        return np.array([f1, f2, f3])
    
    
    @classmethod
    def solution_generate(cls):
        interval = cls.upper_bound[:2] - cls.lower_bound[:2]
        xI = interval*np.random.random(2) + cls.lower_bound[:2]
        h = lambda j,x1,x2:2*x2*np.sin(2*np.pi*x1 + j*np.pi/cls.variable_num)
        xj = [h(j,xI[0], xI[1]) for j in range(3, cls.variable_num+1)]
        return np.array(list(xI) + xj)
