from __future__ import division

import abc

import numpy as np

from . import BaseBenchmark, StaticMOPMixin
from . import check_xval, increment_fevals, accumulate_time


import wfg


__all__ = [ "WFG1",
            "WFG2",
            "WFG3",
            "WFG4",
            "WFG5",
            "WFG6",
            "WFG7",
            "WFG8",
            "WFG9",
            "WFGBaseBenchmark" ]



class WFGBaseBenchmark(BaseBenchmark, StaticMOPMixin):
    """Base class for WFG benchmark test problem"""

    distance = 20
    position = 4
    objective_num = 2
    variable_num = distance + position
    lower_bound = np.array(variable_num*[0.0])
    upper_bound = np.array([2.0*(i+1) for i in range(variable_num)])

    @classmethod
    def solution_generate(cls):
        raise NotImplementedError("WFGBaseBenchmark solution_generate function")


class WFG1(WFGBaseBenchmark):
    func, _ = wfg.create_instance_wfg1(WFGBaseBenchmark.distance, 
            WFGBaseBenchmark.position, WFGBaseBenchmark.objective_num)

    """WFG1 Multi-objective optimization benchmark problem"""
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        return np.array(cls.func(x))


class WFG2(WFGBaseBenchmark):
    func, _ = wfg.create_instance_wfg2(WFGBaseBenchmark.distance, 
            WFGBaseBenchmark.position, WFGBaseBenchmark.objective_num)

    """WFG2 Multi-objective optimization benchmark problem"""
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        return np.array(cls.func(x))


class WFG3(WFGBaseBenchmark):
    func, _ = wfg.create_instance_wfg3(WFGBaseBenchmark.distance, 
            WFGBaseBenchmark.position, WFGBaseBenchmark.objective_num)

    """WFG3 Multi-objective optimization benchmark problem"""
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        return np.array(cls.func(x))


class WFG4(WFGBaseBenchmark):
    func, _ = wfg.create_instance_wfg4(WFGBaseBenchmark.distance, 
            WFGBaseBenchmark.position, WFGBaseBenchmark.objective_num)

    """WFG4 Multi-objective optimization benchmark problem"""
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        return np.array(cls.func(x))


class WFG5(WFGBaseBenchmark):
    func, _ = wfg.create_instance_wfg5(WFGBaseBenchmark.distance, 
            WFGBaseBenchmark.position, WFGBaseBenchmark.objective_num)

    """WFG5 Multi-objective optimization benchmark problem"""
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        return np.array(cls.func(x))


class WFG6(WFGBaseBenchmark):
    func, _ = wfg.create_instance_wfg6(WFGBaseBenchmark.distance, 
            WFGBaseBenchmark.position, WFGBaseBenchmark.objective_num)

    """WFG6 Multi-objective optimization benchmark problem"""
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        return np.array(cls.func(x))


class WFG7(WFGBaseBenchmark):
    func, _ = wfg.create_instance_wfg7(WFGBaseBenchmark.distance, 
            WFGBaseBenchmark.position, WFGBaseBenchmark.objective_num)

    """WFG7 Multi-objective optimization benchmark problem"""
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        return np.array(cls.func(x))


class WFG8(WFGBaseBenchmark):
    func, _ = wfg.create_instance_wfg8(WFGBaseBenchmark.distance, 
            WFGBaseBenchmark.position, WFGBaseBenchmark.objective_num)

    """WFG8 Multi-objective optimization benchmark problem"""
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        return np.array(cls.func(x))


class WFG9(WFGBaseBenchmark):
    func, _ = wfg.create_instance_wfg9(WFGBaseBenchmark.distance, 
            WFGBaseBenchmark.position, WFGBaseBenchmark.objective_num)

    """WFG9 Multi-objective optimization benchmark problem"""
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        return np.array(cls.func(x))
