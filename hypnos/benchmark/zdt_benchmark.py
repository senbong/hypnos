from __future__ import division

import abc

import numpy as np

from . import BaseBenchmark, StaticMOPMixin
from . import check_xval, increment_fevals, accumulate_time



__all__ = [ "ZDT1",
            "ZDT2",
            "ZDT3",
            "ZDT4",
            "ZDT6",
            "ZDTBaseBenchmark" ]



class ZDTBaseBenchmark(BaseBenchmark, StaticMOPMixin):
    """Base class for ZDT benchmark test problem"""

    objective_num = 2
    variable_num = 30
    lower_bound = np.array(30*[0.0])
    upper_bound = np.array(30*[1.0])

    @classmethod
    def solution_generate(cls):
        interval = cls.upper_bound[0] - cls.lower_bound[0]
        x1 = interval*np.random.random() + cls.lower_bound[0]
        return np.array([x1] + (cls.variable_num-1)*[0.0])



class ZDT1(ZDTBaseBenchmark):
    """ZDT1 Multi-objective optimization benchmark problem"""
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        f1 = x[0]
        g = 1.0 + (9.0/(cls.variable_num - 1))*np.sum(x[1:])
        f2 = g*(1 - np.sqrt(f1/g))
        return np.array([f1, f2])



class ZDT2(ZDTBaseBenchmark):
    """ZDT2 Multi-objective optimization benchmark problem"""
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        f1 = x[0]
        g = 1.0 + (9.0/(cls.variable_num - 1))*np.sum(x[1:])
        f2 = g*(1 - np.power(f1/g, 2))
        return np.array([f1, f2])
    


class ZDT3(ZDTBaseBenchmark):
    """ZDT3 Multi-objective optimization benchmark problem"""
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        f1 = x[0]
        g = 1.0 + (9.0/(cls.variable_num - 1))*np.sum(x[1:])
        f2 = g*(1 - np.sqrt(f1/g) - (f1/g)*np.sin(10*np.pi*f1))
        return np.array([f1, f2])
    


class ZDT4(ZDTBaseBenchmark):
    """ZDT4 Multi-objective optimization benchmark problem"""
    variable_num = 10
    lower_bound = np.array([0.0] + 9*[-5.0])
    upper_bound = np.array([1.0] + 9*[5.0])
    
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        f1 = x[0]
        g = 1 + 9*cls.variable_num + np.sum((x[i]**2 - 10*np.cos(4*np.pi*x[i])) 
                for i in range(1, cls.variable_num))
        f2 = g*(1 - np.sqrt(f1/g))
        return np.array([f1, f2])
    
    

class ZDT6(ZDTBaseBenchmark):
    """ZDT6 Multi-objective optimization benchmark problem"""
    variable_num = 10
    lower_bound = np.array(10*[0.0])
    upper_bound = np.array(10*[1.0])
    
    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x):
        f1 = 1.0 - np.exp(-4.0*x[0])*(np.sin(6.0*np.pi*x[0]))**6
        g = 1 + 9*(np.sum(x[1:])/(cls.variable_num - 1))**0.25
        f2 = g*(1.0 - (f1/g)**2)
        return np.array([f1, f2])
