import os
import abc
import time
import functools

import numpy as np

from ..util import validation


__all__ = [ "check_xval",
            "increment_fevals",
            "accumulate_time",
            "AbstractBenchmark",
            "BaseBenchmark",
            "StaticMOPMixin",
            "DynamicMOPMixin" ]



def check_xval(func):
    """Decorator to check input value of x

    Notes
    -----
    It is used to decorate objective_function method.
    """
    @functools.wraps(func)
    def check(cls, x, *args, **kwargs):
        if len(x) != len(cls.lower_bound):
            raise ValueError("Wrong x dimensions")
        
        x = np.array(x)
        if not (np.all(cls.lower_bound <= x) and np.all(x <= cls.upper_bound)):
            raise ValueError("Infeasible x values")
        return func(cls, x, *args, **kwargs)
    return check


def increment_fevals(func):
    """Decorator to increment total number of fitness evaluations

    Notes
    -----
    It is used to decorate objective_function method.
    """
    @functools.wraps(func)
    def increment(cls, *args, **kwargs):
        cls.fitness_eval_num += 1
        return func(cls, *args, **kwargs)
    return increment
        
        
def accumulate_time(func):
    """Decorator to accumulate execution time of objective functions

    Notes
    -----
    It is used to decorate objective_function method.
    """
    @functools.wraps(func)
    def accumulate(cls, *args, **kwargs):
        time_start = time.time()
        output = func(cls, *args, **kwargs)
        cls.total_exec_time += (time.time() - time_start)
        return output
    return accumulate
        


class AbstractBenchmark(object):
    """Interface for Benchmark multi-objective optimization problem
    """

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def objective_function(self, x, *args, **kwargs):
        """Objective function to be optimized
        """
        pass


    @abc.abstractmethod
    def solution_generate(cls, *args, **kwargs):
        """Interface to generate Pareto optimal solution
        """
        pass



class BaseBenchmark(AbstractBenchmark):
    """Base class for benchmark problem
    """

    fitness_eval_num = 0
    total_exec_time = 0.0
    solution_directory = None

    @abc.abstractproperty
    def objective_num(self):
        """Number of objective functions
        """
        pass

    
    @abc.abstractproperty
    def variable_num(self):
        """Number of input dimensions
        """
        pass


    @abc.abstractproperty
    def lower_bound(self):
        """Lower bound of the input to the problem
        """
        pass

    
    @abc.abstractproperty
    def upper_bound(self):
        """Upper bound of the input to the problem
        """
        pass


    @classmethod
    def pof_filename(cls, *args, **kwargs):
        """Filename of the Pareto optimal front
        """
        if cls.solution_directory is None:
            raise validation.PathNotSetError
        if cls._type == "static" or cls._pof_type == "static":
            return os.path.join(cls.solution_directory, "{}_pof.npy".format(cls.__name__))
        else:
            raise NotImplementedError


    @classmethod
    def pos_filename(cls, *args, **kwargs):
        """Filename of the Pareto optimal set
        """
        if cls.solution_directory is None:
            raise validation.PathNotSetError
        if cls._type == "static" or cls._pos_type == "static":
            return os.path.join(cls.solution_directory, "{}_pos.npy".format(cls.__name__))
        else:
            raise NotImplementedError


    @classmethod
    def pareto_front(cls, *args, **kwargs):
        return np.load(cls.pof_filename(*args, **kwargs))


    @classmethod
    def pareto_set(cls, *args, **kwargs):
        return np.load(cls.pos_filename(*args, **kwargs))


    @classmethod
    def reset(cls):
        cls.fitness_eval_num = 0
        cls.total_exec_time = 0



class StaticMOPMixin(object):
    """This mixin is used to set the type of the benchmark problem to static.

    Notes
    -----
    This mixin class is used with AbstractBenchmark class
    """
    _type = "static"



class DynamicMOPMixin(object):
    """This mixin is used to set the type of the benchmark problem to dynamic.

    Notes
    -----
    This mixin class is used with AbstractBenchmark class
    """
    _type = "dynamic"
    _pof_type = None
    _pos_type = None
