from __future__ import division

import os
import abc

import numpy as np

from ..util import validation
from . import BaseBenchmark, DynamicMOPMixin
from . import check_xval, increment_fevals, accumulate_time



__all__ = [ "FDA1",
            "FDA3",
            "FDA4",
            "FDA5",
            "FDABaseBenchmark" ]



class FDABaseBenchmark(BaseBenchmark, DynamicMOPMixin):
    """Base class for FDA dynamic benchmark test problem"""

    fixed_generation = 5
    distinct_step = 10

    @classmethod
    def _gfunction(cls, time):
        return np.sin(0.5*np.pi*time)


    @classmethod
    def _hfunction(cls, f1, g):
        return 1.0 - np.sqrt(f1/g)

    
    @classmethod
    def _generation_to_time(cls, generation):
        return (1.0/cls.distinct_step)*np.floor(generation/cls.fixed_generation) % 1.0


    @classmethod
    def solution_generate(cls, generation):
        time = cls._generation_to_time(generation)
        interval = cls.upper_bound[0] - cls.lower_bound[0]
        x1 = interval*np.random.random() + cls.lower_bound[0]
        xj = [cls._gfunction(time) for _ in range(cls.variable_num - 1)]
        return np.array([x1] + xj)


    @classmethod
    def pof_filename(cls, generation=None):
        if cls.solution_directory is None:
            raise validation.PathNotSetError
        if cls._pof_type == "static":
            return super(FDABaseBenchmark, cls).pof_filename()
        elif type(generation) is not int:
            raise ValueError("Value of generation must be integer.")
        else:
            return os.path.join(cls.solution_directory, "{}_t{:.1f}_pof.npy".format(cls.__name__,
                cls._generation_to_time(generation)))


    @classmethod
    def pos_filename(cls, generation=None):
        if cls.solution_directory is None:
            raise validation.PathNotSetError
        if cls._pos_type == "static":
            return super(FDABaseBenchmark, cls).pos_filename()
        elif type(generation) is not int:
            raise ValueError("Value of generation must be integer.")
        else:
            return os.path.join(cls.solution_directory, "{}_t{:.1f}_pos.npy".format(cls.__name__,
                cls._generation_to_time(generation)))



class FDA1(FDABaseBenchmark):
    """FDA1 Dynamic multi-objective optimization benchmark problem"""

    objective_num = 2
    variable_num = 20
    lower_bound = np.array([0.0] + 19*[-1.0])
    upper_bound = np.array(20*[1.0])
    _pof_type = "static"
    _pos_type = "dynamic"

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        f1 = x[0]
        g = 1.0 + np.sum([(_x - cls._gfunction(time))**2 for _x in x[1:]])
        h = cls._hfunction(f1, g)
        f2 = g*h
        return np.array([f1, f2])



class FDA3(FDABaseBenchmark):
    """FDA3 Dynamic multi-objective optimization benchmark problem"""

    objective_num = 2
    variable_num = 30
    lower_bound = np.array(5*[0.0] + 25*[-1.0])
    upper_bound = np.array(30*[1.0])
    _pof_type = "dynamic"
    _pos_type = "dynamic"

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        f_t = 10**(2*cls._gfunction(time))
        f1 = np.sum(_x**f_t for _x in x[:5])
        g_t = 1 + np.fabs(cls._gfunction(time)) + \
                np.sum((_x - np.fabs(cls._gfunction(time)))**2 for _x in x[5:])
        h = cls._hfunction(f1, g_t)
        f2 = g_t*h
        return np.array([f1, f2])


    @classmethod
    def solution_generate(cls, generation):
        time = cls._generation_to_time(generation)
        interval = cls.upper_bound[:5] - cls.lower_bound[:5]
        xI = interval*np.random.random(5) + cls.lower_bound[:5]
        xj = [cls._gfunction(time) for _ in range(cls.variable_num - 5)]
        return np.array(list(xI) + xj)



class FDA4(FDABaseBenchmark):
    """FDA4 Dynamic multi-objective optimization benchmark problem"""

    objective_num = 3
    variable_num = objective_num + 9
    lower_bound = np.array(variable_num*[0.0])
    upper_bound = np.array(variable_num*[1.0])
    _pof_type = "static"
    _pos_type = "dynamic"

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        g_t = np.fabs(cls._gfunction(time))
        g_x = np.sum((_x - g_t)**2 for _x in x[-10:])
        
        objectives = cls.objective_num*[1.0]
        for i in range(cls.objective_num):
            for j,_x in enumerate(x[:cls.objective_num-1]):
                if j < cls.objective_num - i - 1:
                    objectives[i] *= np.cos(0.5*_x*np.pi)

                if j == cls.objective_num - i - 1:
                    objectives[i] *= np.sin(0.5*_x*np.pi)

            objectives[i] *= (1 + g_x)
        return np.array(objectives)


    @classmethod
    def solution_generate(cls, generation):
        time = cls._generation_to_time(generation)
        interval = cls.upper_bound[:cls.objective_num-1] - cls.lower_bound[:cls.objective_num-1]
        xI = interval*np.random.random(cls.objective_num-1) + cls.lower_bound[:cls.objective_num-1]
        xj = [np.fabs(cls._gfunction(time)) for _ in range(cls.variable_num - cls.objective_num + 1)]
        return np.array(list(xI) + xj)



class FDA5(FDABaseBenchmark):
    """FDA5 Dynamic multi-objective optimization benchmark problem"""

    objective_num = 3
    variable_num = objective_num + 9
    lower_bound = np.array(variable_num*[0.0])
    upper_bound = np.array(variable_num*[1.0])
    _pof_type = "dynamic"
    _pos_type = "dynamic"

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        g_t = np.fabs(cls._gfunction(time))
        g_x = g_t + np.sum((_x - g_t)**2 for _x in x[-10:])
        f_t = 1 + 100*(cls._gfunction(time))**4
        
        objectives = cls.objective_num*[1.0]
        for i in range(cls.objective_num):
            for j,_x in enumerate(x[:cls.objective_num-1]):
                y = _x**f_t
                if j < cls.objective_num - i - 1:
                    objectives[i] *= np.cos(0.5*y*np.pi)

                if j == cls.objective_num - i - 1:
                    objectives[i] *= np.sin(0.5*y*np.pi)

            objectives[i] *= (1 + g_x)
        return np.array(objectives)


    @classmethod
    def solution_generate(cls, generation):
        time = cls._generation_to_time(generation)
        interval = cls.upper_bound[:cls.objective_num-1] - cls.lower_bound[:cls.objective_num-1]
        xI = interval*np.random.random(cls.objective_num-1) + cls.lower_bound[:cls.objective_num-1]
        xj = [np.fabs(cls._gfunction(time)) for _ in range(cls.variable_num - cls.objective_num + 1)]
        return np.array(list(xI) + xj)
