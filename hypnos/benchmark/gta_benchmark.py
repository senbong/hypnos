from __future__ import division

import os
import fractions

import numpy as np

from ..util import validation
from . import BaseBenchmark, DynamicMOPMixin
from . import check_xval, increment_fevals, accumulate_time



__all__ = [ "GTA1a", "GTA1m",
            "GTA2a", "GTA2m",
            "GTA3a", "GTA3m",
            "GTA4a", "GTA4m",
            "GTA5a", "GTA5m",
            "GTA6a", "GTA6m",
            "GTA7a", "GTA7m",
            "GTA8a", "GTA8m",
            "GTA9a", "GTA9m",
            "GTA10a", "GTA10m",
            "GTA11a", "GTA11m",
            "GTA12a", "GTA12m",
            "GTABaseBenchmark" ]



class GTABaseBenchmark(BaseBenchmark, DynamicMOPMixin):
    """Base class for GTA dynamic benchmark test problem"""

    objective_num = 2
    variable_num = 21
    lower_bound = np.array([0.0] + 20*[-1.0])
    upper_bound = np.array(21*[1.0])
    fixed_generation = 5
    distinct_step = 10
    delta_state = 1
    _pof_period = 10
    _pos_period = 4


    @classmethod
    def _gfunction(cls, x, time):
        return np.sin(0.5*np.pi*(time - x))

    
    @classmethod
    def _kindex(cls, time):
        time_int = cls.delta_state*int(time)
        return int(abs(5.0*(int(time_int/5.0) % 2) - (time_int % 5)))


    @classmethod
    def _fix_numerical_instability(cls, x):
        if np.allclose(0.0, x):
            return 0.0

        if np.allclose(np.sqrt(0.5), x):
            return np.sqrt(0.5)
        return x


    @classmethod
    def _beta_unimodal(cls, x, time):
        beta, count = cls.objective_num*[0.0], cls.objective_num*[0]
        for i in range(cls.objective_num-1, cls.variable_num):
            beta[(i+1)%cls.objective_num] += (x[i] - cls._gfunction(x[0], time))**2
            count[(i+1)%cls.objective_num] += 1
        beta = [(2.0/c)*b for c,b in zip(count, beta)]
        return np.array(beta)


    @classmethod
    def _beta_multimodal(cls, x, time):
        beta, count = cls.objective_num*[0.0], cls.objective_num*[0]
        for i in range(cls.objective_num-1, cls.variable_num):
            temp = 1 + np.abs(np.sin(4*np.pi*(x[i] - cls._gfunction(x[0], time))))
            beta[(i+1)%cls.objective_num] += temp*(x[i] - cls._gfunction(x[0], time))**2
            count[(i+1)%cls.objective_num] += 1
        beta = [(2.0/c)*b for c,b in zip(count, beta)]
        return np.array(beta)

    
    @classmethod
    def _beta_mix(cls, x, time):
        beta, count = cls.objective_num*[0.0], cls.objective_num*[0]
        k = cls._kindex(time)
        for i in range(cls.objective_num-1, cls.variable_num):
            temp = np.cos(2*np.pi*k*(x[i] - cls._gfunction(x[0], time)))
            beta[(i+1)%cls.objective_num] += 1.0 + (x[i] - cls._gfunction(x[0], time))**2 - temp
            count[(i+1)%cls.objective_num] += 1
        beta = [(2.0/c)*b for c,b in zip(count, beta)]
        return np.array(beta)


    @classmethod
    def _alpha_convex(cls, x):
        return np.array([x[0], 1 - np.sqrt(x[0])])


    @classmethod
    def _alpha_discrete(cls, x):
        return np.array([x[0], 1.5 - np.sqrt(x[0]) - 0.5*np.sin(10*np.pi*x[0])])


    @classmethod
    def _alpha_mix(cls, x, time):
        k = cls._kindex(time)
        return np.array([x[0], 1 - np.sqrt(x[0]) + 0.1*k*(1 + np.sin(10*np.pi*x[0]))])


    @classmethod
    def _alpha_conf(cls, x, time):
        k = cls._kindex(time)
        return np.array([x[0], 1 - np.power(x[0], 
            np.log(1 - 0.1*k)/np.log(0.1*k + np.finfo(float).eps))])


    @classmethod
    def _alpha_conf_3obj_type1(cls, x, time):

        k = cls._kindex(time)
        alpha1 = cls._fix_numerical_instability(np.cos(0.5*x[0]*np.pi)*np.cos(0.5*x[1]*np.pi))
        alpha2 = cls._fix_numerical_instability(np.cos(0.5*x[0]*np.pi)*np.sin(0.5*x[1]*np.pi))
        alpha3 = cls._fix_numerical_instability(np.sin(0.5*x[0]*np.pi + 0.25*(k/5.0)*np.pi))
        return np.array([alpha1, alpha2, alpha3])

    
    @classmethod
    def _alpha_conf_3obj_type2(cls, x, time):
        k = cls._kindex(time)
        k_ratio = (5.0 - k)/5.0
        alpha1 = cls._fix_numerical_instability(np.cos(0.5*x[0]*np.pi)*np.cos(0.5*x[1]*np.pi*k_ratio))
        alpha2 = cls._fix_numerical_instability(np.cos(0.5*x[0]*np.pi)*np.sin(0.5*x[1]*np.pi*k_ratio))
        alpha3 = cls._fix_numerical_instability(np.sin(0.5*x[0]*np.pi + 0.25*(k/5.0)*np.pi))
        return np.array([alpha1, alpha2, alpha3])


    @classmethod
    def _additive(cls, alpha, beta):
        return alpha + beta


    @classmethod
    def _multiplicative(cls, alpha, beta):
        return alpha*(1 + beta)


    @classmethod
    def solution_generate(cls, generation):
        time = cls._generation_to_time(generation)
        interval = cls.upper_bound[0:cls.objective_num-1] - cls.lower_bound[0:cls.objective_num-1]
        xI = interval*np.random.random(cls.objective_num-1) + cls.lower_bound[0:cls.objective_num-1]
        xj = [cls._gfunction(xI[0], time) for _ in range(cls.variable_num - cls.objective_num + 1)]
        return np.array(list(xI) + xj)


    @classmethod
    def _generation_to_time(cls, generation):
        period = cls._pof_period*cls._pos_period/fractions.gcd(cls._pof_period, cls._pos_period)
        return (1.0/cls.distinct_step)*np.floor(generation/cls.fixed_generation) % period


    @classmethod
    def pof_filename(cls, generation=None):
        if cls.solution_directory is None:
            raise validation.PathNotSetError
        if cls._pof_type == "static":
            return os.path.join(cls.solution_directory, "{}_pof.npy".format(cls.__name__[:-1]))
        elif type(generation) is not int:
            raise ValueError("Value of generation must be integer.")
        else:
            return os.path.join(cls.solution_directory, "{}_t{:.1f}_pof.npy".format(
                cls.__name__[:-1], cls._generation_to_time(generation)))


    @classmethod
    def pos_filename(cls, generation=None):
        if cls.solution_directory is None:
            raise validation.PathNotSetError
        if cls._pos_type == "static":
            return os.path.join(cls.solution_directory, "{}_pos.npy".format(cls.__name__[:-1]))
        elif type(generation) is not int:
            raise ValueError("Value of generation must be integer.")
        else:
            return os.path.join(cls.solution_directory, "{}_t{:.1f}_pos.npy".format(
                cls.__name__[:-1], cls._generation_to_time(generation)))



class GTA1a(GTABaseBenchmark):
    """GTA1a Dynamic multi-objective optimization benchmark problem"""
    
    _pof_type = "static"
    _pos_type = "dynamic"
    _pof_period = 1

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._additive(cls._alpha_convex(x), cls._beta_unimodal(x, time))



class GTA1m(GTABaseBenchmark):
    """GTA1m Dynamic multi-objective optimization benchmark problem"""
    
    _pof_type = "static"
    _pos_type = "dynamic"
    _pof_period = 1

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._multiplicative(cls._alpha_convex(x), cls._beta_unimodal(x, time))



class GTA2a(GTABaseBenchmark):
    """GTA2a Dynamic multi-objective optimization benchmark problem"""

    _pof_type = "static"
    _pos_type = "dynamic"
    _pof_period = 1

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._additive(cls._alpha_convex(x), cls._beta_multimodal(x, time))



class GTA2m(GTABaseBenchmark):
    """GTA2m Dynamic multi-objective optimization benchmark problem"""

    _pof_type = "static"
    _pos_type = "dynamic"
    _pof_period = 1

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._multiplicative(cls._alpha_convex(x), cls._beta_multimodal(x, time))



class GTA3a(GTABaseBenchmark):
    """GTA3a Dynamic multi-objective optimization benchmark problem"""

    _pof_type = "static"
    _pos_type = "dynamic"
    _pof_period = 1

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._additive(cls._alpha_convex(x), cls._beta_mix(x, time))



class GTA3m(GTABaseBenchmark):
    """GTA3m Dynamic multi-objective optimization benchmark problem"""

    _pof_type = "static"
    _pos_type = "dynamic"
    _pof_period = 1

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._multiplicative(cls._alpha_convex(x), cls._beta_mix(x, time))



class GTA4a(GTABaseBenchmark):
    """GTA4a Dynamic multi-objective optimization benchmark problem"""

    _pof_type = "static"
    _pos_type = "dynamic"
    _pof_period = 1

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._additive(cls._alpha_discrete(x), cls._beta_mix(x, time))



class GTA4m(GTABaseBenchmark):
    """GTA4m Dynamic multi-objective optimization benchmark problem"""

    _pof_type = "static"
    _pos_type = "dynamic"
    _pof_period = 1

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._multiplicative(cls._alpha_discrete(x), cls._beta_mix(x, time))



class GTA5a(GTABaseBenchmark):
    """GTA5a Dynamic multi-objective optimization benchmark problem"""

    _pof_type = "dynamic"
    _pos_type = "dynamic"

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._additive(cls._alpha_mix(x, time), cls._beta_multimodal(x, time))



class GTA5m(GTABaseBenchmark):
    """GTA5m Dynamic multi-objective optimization benchmark problem"""

    _pof_type = "dynamic"
    _pos_type = "dynamic"

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._multiplicative(cls._alpha_mix(x, time), cls._beta_multimodal(x, time))



class GTA6a(GTABaseBenchmark):
    """GTA6a Dynamic multi-objective optimization benchmark problem"""

    _pof_type = "dynamic"
    _pos_type = "dynamic"

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._additive(cls._alpha_mix(x, time), cls._beta_mix(x, time))



class GTA6m(GTABaseBenchmark):
    """GTA6m Dynamic multi-objective optimization benchmark problem"""

    _pof_type = "dynamic"
    _pos_type = "dynamic"

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._multiplicative(cls._alpha_mix(x, time), cls._beta_mix(x, time))



class GTA7a(GTABaseBenchmark):
    """GTA7a Dynamic multi-objective optimization benchmark problem"""

    _pof_type = "dynamic"
    _pos_type = "dynamic"

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._additive(cls._alpha_conf(x, time), cls._beta_multimodal(x, time))



class GTA7m(GTABaseBenchmark):
    """GTA7m Dynamic multi-objective optimization benchmark problem"""

    _pof_type = "dynamic"
    _pos_type = "dynamic"

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._multiplicative(cls._alpha_conf(x, time), cls._beta_multimodal(x, time))



class GTA8a(GTABaseBenchmark):
    """GTA8a Dynamic multi-objective optimization benchmark problem"""

    _pof_type = "dynamic"
    _pos_type = "dynamic"

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._additive(cls._alpha_conf(x, time), cls._beta_mix(x, time))



class GTA8m(GTABaseBenchmark):
    """GTA8m Dynamic multi-objective optimization benchmark problem"""

    _pof_type = "dynamic"
    _pos_type = "dynamic"

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._multiplicative(cls._alpha_conf(x, time), cls._beta_mix(x, time))



class GTA9a(GTABaseBenchmark):
    """GTA9a Dynamic multi-objective optimization benchmark problem"""

    objective_num = 3
    _pof_type = "dynamic"
    _pos_type = "dynamic"

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._additive(cls._alpha_conf_3obj_type1(x, time), cls._beta_multimodal(x, time))



class GTA9m(GTABaseBenchmark):
    """GTA9m Dynamic multi-objective optimization benchmark problem"""

    objective_num = 3
    _pof_type = "dynamic"
    _pos_type = "dynamic"

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._multiplicative(cls._alpha_conf_3obj_type1(x, time), cls._beta_multimodal(x, time))



class GTA10a(GTABaseBenchmark):
    """GTA10a Dynamic multi-objective optimization benchmark problem"""

    objective_num = 3
    _pof_type = "dynamic"
    _pos_type = "dynamic"

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._additive(cls._alpha_conf_3obj_type1(x, time), cls._beta_mix(x, time))



class GTA10m(GTABaseBenchmark):
    """GTA10m Dynamic multi-objective optimization benchmark problem"""

    objective_num = 3
    _pof_type = "dynamic"
    _pos_type = "dynamic"

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._multiplicative(cls._alpha_conf_3obj_type1(x, time), cls._beta_mix(x, time))



class GTA11a(GTABaseBenchmark):
    """GTA11a Dynamic multi-objective optimization benchmark problem"""

    objective_num = 3
    _pof_type = "dynamic"
    _pos_type = "dynamic"

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._additive(cls._alpha_conf_3obj_type2(x, time), cls._beta_multimodal(x, time))



class GTA11m(GTABaseBenchmark):
    """GTA11m Dynamic multi-objective optimization benchmark problem"""

    objective_num = 3
    _pof_type = "dynamic"
    _pos_type = "dynamic"

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._multiplicative(cls._alpha_conf_3obj_type2(x, time), cls._beta_multimodal(x, time))



class GTA12a(GTABaseBenchmark):
    """GTA12a Dynamic multi-objective optimization benchmark problem"""

    objective_num = 3
    _pof_type = "dynamic"
    _pos_type = "dynamic"

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._additive(cls._alpha_conf_3obj_type2(x, time), cls._beta_mix(x, time))



class GTA12m(GTABaseBenchmark):
    """GTA12m Dynamic multi-objective optimization benchmark problem"""

    objective_num = 3
    _pof_type = "dynamic"
    _pos_type = "dynamic"

    @classmethod
    @check_xval
    @accumulate_time
    @increment_fevals
    def objective_function(cls, x, generation):
        time = cls._generation_to_time(generation)
        return cls._multiplicative(cls._alpha_conf_3obj_type2(x, time), cls._beta_mix(x, time))
