## Hypnos

Hypnos is a lightweight evolutionary multi-objective selection library written in Python. The selection operator is implemented using coroutine which benefits asynchronous programming.

## Code Example

```python
import sys
sys.path.append("~/hypnos")  # add path to project root

import numpy as np
import hypnos.benchmark as benchmark
from hypnos.selection import DominationSelection
from hypnos.metaheuristics import BoxBounder,SimulatedBinaryXover,PolynomialMutation,OneOffspringVariator

# a non-dominated sorting genetic algorithm
mop = benchmark.ZDT1
mop.reset() # reset execution time record
nobj, nvar = mop.objective_num, mop.variable_num
population_size, max_gen = 100, 100
selector = DominationSelection(nobj=nobj, population_size=population_size)
bounder = BoxBounder(method="clip", lower_bound=mop.lower_bound, upper_bound=mop.upper_bound)
xover = SimulatedBinaryXover(bounder=bounder, eta_c=20)
mutator = PolynomialMutation(bounder=bounder, eta_m=20, mutation_rate=1.0/nvar)
variator = OneOffspringVariator(selector=selector, xover=xover, mutator=mutator)

# prepare for evolutionary search
for i in range(max_gen):
    for _ in range(selector.population_size):

        if i == 0:
            # random initialization
            interval = mop.upper_bound - mop.lower_bound
            variable = np.random.random(nvar)*interval + mop.lower_bound
        else:
            # crossover and mutation
            variable = variator.generate()

        # evaluate variable and feed to the selector
        objective = mop.objective_function(variable)
        selector.feed(variable, objective)

    # display message
    if (i+1) % max(max_gen//5, 1) == 0:
        print("Generation {:>3}, total fit. evals. {:>5}, total obj. exec. time {:<10}".format(
                i+1, mop.fitness_eval_num, mop.total_exec_time))

# plot the output solutions
selector.plot()
```
