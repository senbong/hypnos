from __future__ import division

import os
import sys
import tempfile
import cPickle as pickle
from unittest import TestCase, main

# third-party package
import numpy as np

# add path to the root project
testdir = os.path.dirname(__file__)
sys.path.insert(0, os.path.join(testdir, '..'))

from hypnos.selection import ObjectiveVariableMap, DecompositionSelection, \
        GeometryBasedDecompositionSelection, \
        Individual, DominationSelection


class ObjectiveVariableMapTest(TestCase):

    def test_functions(self):
        instance = ObjectiveVariableMap(minimization=True)
        self.assertEqual(instance.is_initialized(), False)
        instance.variable = 1.0
        self.assertEqual(instance.is_initialized(), True)
        instance.reset()
        self.assertEqual(instance.is_initialized(), False)
    


class DecompositionSelectionTest(TestCase):

    def test_constructor(self):
        nobj, interval_num, neighbor_num = 2, 5, 2
        with self.assertRaises(ValueError):
            DecompositionSelection(nobj, interval_num, 
                neighbor_num, minimization=True, n2p_ratio=1.1)

        with self.assertRaises(ValueError):
            DecompositionSelection(nobj, interval_num, 
                neighbor_num, minimization=True, n2p_ratio=-1.1)

        selector = DecompositionSelection(nobj, interval_num, neighbor_num)
        self.assertEqual(selector.objective_num, nobj)
        self.assertEqual(selector.interval_num, interval_num)
        self.assertEqual(selector.neighbor_num, neighbor_num)
        
        # test the generated weight vectors
        for p in selector.population:
            self.assertEqual(sum(p.weight), 1.0)

        self.assertEqual(selector.evals_num, 0)
        self.assertEqual(selector.generation_num, 0)


    def test_update_reference(self):
        nobj, interval_num, neighbor_num = 2, 5, 2
        selector = DecompositionSelection(nobj, interval_num, neighbor_num, minimization=True)
        objectives = np.array([10, 10])
        selector._update_reference(objectives)
        self.assertTrue(np.all(selector.ideal_vector == objectives))

        objectives = np.array([5, 20])
        selector._update_reference(objectives)
        self.assertTrue(np.all(selector.ideal_vector == np.array([5, 10])))

        objectives = np.array([20, 5])
        selector._update_reference(objectives)
        self.assertTrue(np.all(selector.ideal_vector == np.array([5, 5])))

        objectives = np.array([20, 20])
        selector._update_reference(objectives)
        self.assertTrue(np.all(selector.ideal_vector == np.array([5, 5])))

        selector = DecompositionSelection(nobj, interval_num, neighbor_num, minimization=False)
        objectives = np.array([10, 10])
        selector._update_reference(objectives)
        self.assertTrue(np.all(selector.ideal_vector == objectives))

        objectives = np.array([5, 20])
        selector._update_reference(objectives)
        self.assertTrue(np.all(selector.ideal_vector == np.array([10, 20])))

        objectives = np.array([20, 5])
        selector._update_reference(objectives)
        self.assertTrue(np.all(selector.ideal_vector == np.array([20, 20])))


    def test_feed(self):
        nobj, interval_num, neighbor_num = 2, 5, 2
        selector = DecompositionSelection(nobj, interval_num, neighbor_num)
        selector.feed(0, [0, 0])
        with self.assertRaises(ValueError):
            selector.feed(0, [0, 0, 0])

        with self.assertRaises(ValueError):
            selector.feed(None, [0, 0])


    def test_mating_index(self):
        nobj, interval_num, neighbor_num, n2p_ratio = 2, 5, 2, 0.8
        selector = DecompositionSelection(nobj, interval_num, neighbor_num, n2p_ratio=n2p_ratio)
        index = 0
        neighborhood = selector.neighbor_map[index]
        neighbor_count, total_count = 0, 1000
        for i in range(total_count):
            parents = selector.mating_index(index, parent_size=2)
            if all([p in neighborhood for p in parents]):
                neighbor_count += 1

        estimated_n2p = neighbor_count / total_count
        self.assertTrue(0.8*n2p_ratio <= estimated_n2p <= 1.2*n2p_ratio)


    def test_plot(self):
        nobj, interval_num, neighbor_num = 2, 5, 2
        selector = DecompositionSelection(nobj, interval_num, neighbor_num)
        
        with self.assertRaises(ValueError):
            selector.plot(dims=[0])

        with self.assertRaises(ValueError):
            selector.plot(dims=[1, 3])


    def test_save(self):
        nobj, interval_num, neighbor_num = 2, 5, 2
        selector = DecompositionSelection(nobj, interval_num, neighbor_num)
        objectives = np.random.random((3, 2))
        for i,obj in enumerate(objectives):
            selector.population[i].objective = obj
            selector.population[i].variable = i

        temp_filename = None
        with tempfile.NamedTemporaryFile(delete=False) as fhandle:
            temp_filename = fhandle.name
            selector.save(fhandle)

        with open(temp_filename) as fhandle:
            state = pickle.load(fhandle)
        
        os.unlink(temp_filename)

        for i,obj in enumerate(objectives):
            self.assertTrue(np.all(selector.population[i].objective == state[i]["objective"]))
            self.assertTrue(selector.population[i].variable == state[i]["variable"])
            state.pop(i, None)

        self.assertEqual(len(state), 0)



class GeometryBasedDecompositionSelectionTest(TestCase):

    def test_constructor(self):
        nobj, interval_num, neighbor_num = 2, 5, 2
        with self.assertRaises(ValueError):
            GeometryBasedDecompositionSelection(nobj, interval_num, 
                neighbor_num, minimization=True, mrdl_threshold=-0.1)



class IndividualTest(TestCase):

    def test_dominate(self):
        ind1 = Individual(minimization=True)
        ind2 = Individual(minimization=True)
        ind1.objective = [0.0, 0.5]
        ind2.objective = [1.0, 1.0]
        self.assertTrue(ind1.dominate(ind2))
        self.assertEqual(ind1.dominate(ind1), False)
        self.assertEqual(ind2.dominate(ind1), False)
        ind2.objective = [0.5, 0.0]
        self.assertEqual(ind1.dominate(ind2), False)
        self.assertEqual(ind2.dominate(ind1), False)



class DominationSelectionTest(TestCase):

    def setUp(self):
        nobj, population_size = 2, 5
        self.selector = DominationSelection(nobj, population_size=population_size)


    def test_constructor(self):
        with self.assertRaises(AttributeError):
            self.selector.population_size = 10


    def test_nondominated_sorting(self):
        objective_list = [[0.1*i, 1.0-0.1*i] for i in range(5)] + \
                            [[0.1*i+0.1, 1.1-0.1*i] for i in range(5)]
        first_front = [tuple(obj) for i,obj in enumerate(objective_list) if i < 5]
        second_front = [tuple(obj) for i,obj in enumerate(objective_list) if i >= 5]

        population = list()
        for obj in objective_list:
            ind = Individual(minimization=True)
            ind.objective = obj
            population.append(ind)

        fronts = self.selector._nondominated_sorting(population) 
        front_set = [tuple(ind.objective) for ind in fronts[0]]
        self.assertEqual(front_set, first_front)
        front_set = [tuple(ind.objective) for ind in fronts[1]]
        self.assertEqual(front_set, second_front)


    def test_crowding_distance(self):
        objective_list = [[0.25*i, 1.0-0.25*i] for i in range(5)]
        distance_list = [np.finfo(float).max, 1.0, 1.0, 1.0, np.finfo(float).max]
        front = list()
        for obj in objective_list:
            ind = Individual(minimization=True)
            ind.objective = obj
            front.append(ind)

        self.selector._crowding_distance(front)
        for ind,dist in zip(front, distance_list):
            self.assertEqual(ind.crowding_distance, dist)


    def test_mating_index(self):
        objective_list = [[0.1*i, 1.0-0.1*i] for i in range(5)] + \
                            [[0.1*i+0.1, 1.1-0.1*i] for i in range(5)]
        for i,obj in enumerate(objective_list):
            self.selector.feed(i, obj)

        for i in range(2, 5):
            parent_indices = self.selector.mating_index(i)
            self.assertEqual(len(parent_indices), len(set(parent_indices)))



if __name__ == '__main__':

    main()
