from __future__ import division

import os
import sys
import random
from unittest import TestCase, main

# third-party package
import numpy as np

# add path to the root project
testdir = os.path.dirname(__file__)
sys.path.insert(0, os.path.join(testdir, '..'))


from hypnos.metaheuristics import BoxBounder


class BoxBounderTest(TestCase):

    def test_bounder(self):
        
        for method in ["clip", "cycle", "mirror"]:
            bounder = BoxBounder(0.8, 0.5, method=method)
            for i in range(1000):
                x = random.random()
                x = bounder(x)
                self.assertTrue(0.5 <= x <= 0.8)

        ndim = 5
        for method in ["clip", "cycle", "mirror"]:
            bounder = BoxBounder(0.8*np.ones(ndim), 0.5*np.ones(ndim), method=method)
            for i in range(1000):
                x = np.random.random(ndim)
                x = bounder(x)
                self.assertTrue(np.all(0.5 <= x) and np.all(x <= 0.8))

        bounder = BoxBounder(0.8, 0.5, "clip")
        self.assertTrue(bounder(0.3) == 0.5)
        self.assertTrue(bounder(1.0) == 0.8)

        bounder = BoxBounder(0.8, 0.5, "cycle")
        self.assertTrue(np.allclose(bounder(0.4), 0.7))
        self.assertTrue(np.allclose(bounder(0.9), 0.6))

        bounder = BoxBounder(0.8, 0.5, "mirror")
        self.assertTrue(np.allclose(bounder(0.4), 0.6))
        self.assertTrue(np.allclose(bounder(0.9), 0.7))



if __name__ == '__main__':

    main()
