from __future__ import division

import os
import sys
import fractions
from unittest import TestCase, main, skipIf, skip

# third-party package
import numpy as np

# add path to the root project
testdir = os.path.dirname(__file__)
sys.path.insert(0, os.path.join(testdir, '..'))

import hypnos.benchmark as benchmark
import hypnos.util.validation as validation


class ZDTTest(TestCase):
    
    def test_ZDT_solution_filename(self):
        for test_num in [1, 2, 3, 4, 6]:
            mop = getattr(benchmark, "ZDT{}".format(test_num))
            # test invalid call
            with self.assertRaises(validation.PathNotSetError):
                mop.pof_filename()

            with self.assertRaises(validation.PathNotSetError):
                mop.pos_filename()

            # test normal usage
            mop.solution_directory = ".."
            self.assertEqual(mop.pof_filename(), os.path.join("..", "ZDT{}_pof.npy".format(test_num)))
            self.assertEqual(mop.pos_filename(), os.path.join("..", "ZDT{}_pos.npy".format(test_num)))


    @skipIf(not os.path.isdir(os.path.join(testdir, "..", "data")),
            "solution directory does not exist")
    def test_ZDT_solution_load(self):
        for test_num in [1, 2, 3, 4, 6]:
            mop = getattr(benchmark, "ZDT{}".format(test_num))
            mop.solution_directory = os.path.join(testdir, "..", "data")
            pof = mop.pareto_front()
            pos = mop.pareto_set()
            self.assertEqual(pof.shape[1], mop.objective_num)
            self.assertEqual(pos.shape[1], mop.variable_num)


    def test_ZDT1_solution_generate(self):
        for i in range(30):
            objectives = benchmark.ZDT1.objective_function(benchmark.ZDT1.solution_generate())
            f2 = 1.0 - np.sqrt(objectives[0])
            self.assertTrue(np.allclose(objectives[1], f2))


    def test_ZDT2_solution_generate(self):
        for i in range(30):
            objectives = benchmark.ZDT2.objective_function(benchmark.ZDT2.solution_generate())
            f2 = 1.0 - objectives[0]**2
            self.assertTrue(np.allclose(objectives[1], f2))


    def test_ZDT3_solution_generate(self):
        for i in range(30):
            objectives = benchmark.ZDT3.objective_function(benchmark.ZDT3.solution_generate())
            f2 = 1.0 - np.sqrt(objectives[0]) - objectives[0]*np.sin(10*np.pi*objectives[0])
            self.assertTrue(np.allclose(objectives[1], f2))


    def test_ZDT4_solution_generate(self):
        for i in range(30):
            objectives = benchmark.ZDT4.objective_function(benchmark.ZDT4.solution_generate())
            f2 = 1.0 - np.sqrt(objectives[0])
            self.assertTrue(np.allclose(objectives[1], f2))


    def test_ZDT6_solution_generate(self):
        for i in range(30):
            objectives = benchmark.ZDT6.objective_function(benchmark.ZDT6.solution_generate())
            f2 = 1.0 - objectives[0]**2
            self.assertTrue(np.allclose(objectives[1], f2))



class CEC09Test(TestCase):
    
    def test_CEC09_solution_filename(self):
        for test_num in range(1, 11):
            mop = getattr(benchmark, "UF{}".format(test_num))
            # test invalid call
            with self.assertRaises(validation.PathNotSetError):
                mop.pof_filename()

            with self.assertRaises(validation.PathNotSetError):
                mop.pos_filename()

            # test normal usage
            mop.solution_directory = ".."
            self.assertEqual(mop.pof_filename(), os.path.join("..", "UF{}_pof.npy".format(test_num)))
            self.assertEqual(mop.pos_filename(), os.path.join("..", "UF{}_pos.npy".format(test_num)))


    @skipIf(not os.path.isdir(os.path.join(testdir, "..", "data")),
            "solution directory does not exist")
    def test_CEC09_solution_load(self):
        for test_num in range(1, 11):
            mop = getattr(benchmark, "UF{}".format(test_num))
            mop.solution_directory = os.path.join(testdir, "..", "data")
            pof = mop.pareto_front()
            pos = mop.pareto_set()
            self.assertEqual(pof.shape[1], mop.objective_num)
            self.assertEqual(pos.shape[1], mop.variable_num)


    def test_UF1_solution_generate(self):
        for i in range(30):
            objectives = benchmark.UF1.objective_function(benchmark.UF1.solution_generate())
            f2 = 1.0 - np.sqrt(objectives[0])
            self.assertTrue(np.allclose(objectives[1], f2))


    def test_UF2_solution_generate(self):
        for i in range(30):
            objectives = benchmark.UF2.objective_function(benchmark.UF2.solution_generate())
            f2 = 1.0 - np.sqrt(objectives[0])
            self.assertTrue(np.allclose(objectives[1], f2))
    

    def test_UF3_solution_generate(self):
        for i in range(30):
            objectives = benchmark.UF3.objective_function(benchmark.UF3.solution_generate())
            f2 = 1.0 - np.sqrt(objectives[0])
            self.assertTrue(np.allclose(objectives[1], f2))
    

    def test_UF4_solution_generate(self):
        for i in range(30):
            objectives = benchmark.UF4.objective_function(benchmark.UF4.solution_generate())
            f2 = 1.0 - (objectives[0])**2
            self.assertTrue(np.allclose(objectives[1], f2))


    def test_UF5_solution_generate(self):
        N, epsilon = 10, 0.1
        for i in range(30):
            solution = benchmark.UF5.solution_generate()
            objectives = benchmark.UF5.objective_function(solution)
            f2 = 1.0 - solution[0]
            delta = (1.0/(2*N) + epsilon)*np.fabs(np.sin(2*N*np.pi*solution[0]))
            self.assertTrue(solution[0] <= objectives[0] <= solution[0] + delta)
            self.assertTrue(f2 <= objectives[1] <= f2 + delta)


    def test_UF6_solution_generate(self):
        N, epsilon = 2, 0.1
        for i in range(30):
            solution = benchmark.UF6.solution_generate()
            objectives = benchmark.UF6.objective_function(solution)
            f2 = 1.0 - solution[0]
            delta = max(0, 2*(1/(2*N) + epsilon)*np.sin(2*N*np.pi*solution[0]))
            self.assertTrue(f2 <= objectives[1] <= f2 + delta)


    def test_UF7_solution_generate(self):
        for i in range(30):
            solution = benchmark.UF7.solution_generate()
            objectives = benchmark.UF7.objective_function(benchmark.UF7.solution_generate())
            f2 = 1.0 - objectives[0]
            self.assertEqual(objectives[1], f2)


    def test_UF8_solution_generate(self):
        for i in range(30):
            objectives = benchmark.UF8.objective_function(benchmark.UF8.solution_generate())
            self.assertTrue(np.allclose(np.sum(objectives*objectives), 1.0))


    def test_UF9_solution_generate(self):
        for i in range(30):
            objectives = benchmark.UF9.objective_function(benchmark.UF9.solution_generate())
            delta = 1.0 - objectives[2]
            self.assertTrue((0.0 <= objectives[0] <= 0.25*delta) or (0.75*delta <= objectives[0] <= 1.0))
            self.assertTrue(np.allclose(objectives[1], 1.0 - objectives[0] - objectives[2]))
            self.assertTrue(0.0 <= objectives[2] <= 1.0)


    def test_UF10_solution_generate(self):
        for i in range(30):
            objectives = benchmark.UF10.objective_function(benchmark.UF10.solution_generate())
            self.assertTrue(np.allclose(np.sum(objectives*objectives), 1.0))



class FDATest(TestCase):

    def test_FDA_solution_filename(self):
        for test_num in [1, 3, 4, 5]:
            mop = getattr(benchmark, "FDA{}".format(test_num))
            # test invalid call
            if mop._pof_type == "static":
                with self.assertRaises(validation.PathNotSetError):
                    mop.pof_filename()
            if mop._pof_type == "dynamic":
                with self.assertRaises(validation.PathNotSetError):
                    mop.pof_filename(1)
                with self.assertRaises(ValueError):
                    mop.pof_filename()
            if mop._pos_type == "static":
                with self.assertRaises(validation.PathNotSetError):
                    mop.pos_filename()
            if mop._pos_type == "dynamic":
                with self.assertRaises(validation.PathNotSetError):
                    mop.pos_filename(1)
                with self.assertRaises(ValueError):
                    mop.pos_filename()
            # test normal usage
            mop.solution_directory = ".."
            for n in range(mop.distinct_step):
                generation = n*mop.fixed_generation
                time = mop._generation_to_time(generation)
                if mop._pof_type == "static":
                    self.assertEqual(mop.pof_filename(),
                            os.path.join("..", "FDA{}_pof.npy".format(test_num)))
                    self.assertEqual(mop.pof_filename(generation),
                            os.path.join("..", "FDA{}_pof.npy".format(test_num)))
                if mop._pos_type == "static":
                    self.assertEqual(mop.pos_filename(),
                            os.path.join("..", "FDA{}_pos.npy".format(test_num)))
                    self.assertEqual(mop.pos_filename(generation),
                            os.path.join("..", "FDA{}_pos.npy".format(test_num)))
                if mop._pof_type == "dynamic":
                    self.assertEqual(mop.pof_filename(generation),
                            os.path.join("..", "FDA{}_t{}_pof.npy".format(test_num, time)))
                if mop._pos_type == "dynamic":
                    self.assertEqual(mop.pos_filename(generation),
                            os.path.join("..", "FDA{}_t{}_pos.npy".format(test_num, time)))


    @skipIf(not os.path.isdir(os.path.join(testdir, "..", "data")),
            "solution directory does not exist")
    def test_FDA_solution_load(self):
        for test_num in [1, 3, 4, 5]:
            mop = getattr(benchmark, "FDA{}".format(test_num))
            mop.solution_directory = os.path.join(testdir, "..", "data")
            for n in range(mop.distinct_step):
                generation = n*mop.fixed_generation
                if mop._pof_type == "static":
                    pof = mop.pareto_front()
                    self.assertEqual(pof.shape[1], mop.objective_num)
                    pof = mop.pareto_front(generation)
                    self.assertEqual(pof.shape[1], mop.objective_num)

                if mop._pos_type == "static":
                    pos = mop.pareto_set()
                    self.assertEqual(pos.shape[1], mop.variable_num)
                    pos = mop.pareto_set(generation)
                    self.assertEqual(pos.shape[1], mop.variable_num)

                if mop._pof_type == "dynamic":
                    pof = mop.pareto_front(generation)
                    self.assertEqual(pof.shape[1], mop.objective_num)

                if mop._pos_type == "dynamic":
                    pos = mop.pareto_set(generation)
                    self.assertEqual(pos.shape[1], mop.variable_num)


    def test_FDA1_solution_generate(self):
        for i in range(30):
            for n in range(benchmark.FDA1.distinct_step):
                generation = n*benchmark.FDA1.fixed_generation
                solution = benchmark.FDA1.solution_generate(generation)
                objectives = benchmark.FDA1.objective_function(solution, generation)
                f2 = 1 - np.sqrt(objectives[0])
                self.assertTrue(np.allclose(objectives[1], f2))


    def test_FDA3_solution_generate(self):
        for i in range(30):
            for n in range(benchmark.FDA3.distinct_step):
                generation = n*benchmark.FDA3.fixed_generation
                solution = benchmark.FDA3.solution_generate(generation)
                objectives = benchmark.FDA3.objective_function(solution, generation)
                g = 1 + np.fabs(np.sin(0.5*np.pi*benchmark.FDA3._generation_to_time(generation)))
                f2 = g*(1 - np.sqrt(objectives[0]/g))
                self.assertTrue(np.allclose(objectives[1], f2))


    def test_FDA4_solution_generate(self):
        for i in range(30):
            for n in range(benchmark.FDA4.distinct_step):
                generation = n*benchmark.FDA4.fixed_generation
                solution = benchmark.FDA4.solution_generate(generation)
                objectives = benchmark.FDA4.objective_function(solution, generation)
                self.assertTrue(np.allclose(np.sum(objectives*objectives), 1))


    def test_FDA5_solution_generate(self):
        for i in range(30):
            for n in range(benchmark.FDA5.distinct_step):
                generation = n*benchmark.FDA5.fixed_generation
                solution = benchmark.FDA5.solution_generate(generation)
                objectives = benchmark.FDA5.objective_function(solution, generation)
                g = 1 + np.fabs(np.sin(0.5*np.pi*benchmark.FDA5._generation_to_time(generation)))
                self.assertTrue(np.allclose(np.sum(objectives*objectives), g**2))



@skip("GTA dynamic benchmark is under development...")
class GTATest(TestCase):

    def test_GTA_solution_filename(self):
        for test_num in range(1, 13):
            for test_type in ["a", "m"]:
                mop = getattr(benchmark, "GTA{}{}".format(test_num, test_type))
                # test invalid call
                if mop._pof_type == "static":
                    with self.assertRaises(validation.PathNotSetError):
                        mop.pof_filename()
                if mop._pof_type == "dynamic":
                    with self.assertRaises(validation.PathNotSetError):
                        mop.pof_filename(1)
                    with self.assertRaises(ValueError):
                        mop.pof_filename()
                if mop._pos_type == "static":
                    with self.assertRaises(validation.PathNotSetError):
                        mop.pos_filename()
                if mop._pos_type == "dynamic":
                    with self.assertRaises(validation.PathNotSetError):
                        mop.pos_filename(1)
                    with self.assertRaises(ValueError):
                        mop.pos_filename()
                # test normal usage
                mop.solution_directory = ".."
                for n in range(mop.distinct_step):
                    generation = n*mop.fixed_generation
                    time = mop._generation_to_time(generation)
                    if mop._pof_type == "static":
                        self.assertEqual(mop.pof_filename(),
                                os.path.join("..", "GTA{}_pof.npy".format(test_num)))
                        self.assertEqual(mop.pof_filename(generation),
                                os.path.join("..", "GTA{}_pof.npy".format(test_num)))
                    if mop._pos_type == "static":
                        self.assertEqual(mop.pos_filename(),
                                os.path.join("..", "GTA{}_pos.npy".format(test_num)))
                        self.assertEqual(mop.pos_filename(generation),
                                os.path.join("..", "GTA{}_pos.npy".format(test_num)))
                    if mop._pof_type == "dynamic":
                        self.assertEqual(mop.pof_filename(generation),
                                os.path.join("..", "GTA{}_t{}_pof.npy".format(test_num, time)))
                    if mop._pos_type == "dynamic":
                        self.assertEqual(mop.pos_filename(generation),
                                os.path.join("..", "GTA{}_t{}_pos.npy".format(test_num, time)))


    def test_GTA1_solution_generate(self):
        pof_period, pos_period = benchmark.GTA1a._pof_period, benchmark.GTA1a._pos_period
        period = int(pof_period*pos_period/fractions.gcd(pof_period, pos_period))
        for i in range(30):
            for n in range(benchmark.GTA1a.distinct_step*period):
                generation = n*benchmark.GTA1a.fixed_generation
                # additive
                solution = benchmark.GTA1a.solution_generate(generation)
                objectives = benchmark.GTA1a.objective_function(solution, generation)
                self.assertTrue(np.allclose(objectives[1], 1.0 - np.sqrt(objectives[0])))
                # multiplicative
                solution = benchmark.GTA1m.solution_generate(generation)
                objectives = benchmark.GTA1m.objective_function(solution, generation)
                self.assertTrue(np.allclose(objectives[1], 1.0 - np.sqrt(objectives[0])))


    def test_GTA2_solution_generate(self):
        pof_period, pos_period = benchmark.GTA2a._pof_period, benchmark.GTA2a._pos_period
        period = int(pof_period*pos_period/fractions.gcd(pof_period, pos_period))
        for i in range(30):
            for n in range(benchmark.GTA2a.distinct_step*period):
                generation = n*benchmark.GTA2a.fixed_generation
                # additive
                solution = benchmark.GTA2a.solution_generate(generation)
                objectives = benchmark.GTA2a.objective_function(solution, generation)
                self.assertTrue(np.allclose(objectives[1], 1.0 - np.sqrt(objectives[0])))
                # multiplicative
                solution = benchmark.GTA2m.solution_generate(generation)
                objectives = benchmark.GTA2m.objective_function(solution, generation)
                self.assertTrue(np.allclose(objectives[1], 1.0 - np.sqrt(objectives[0])))


    def test_GTA3_solution_generate(self):
        pof_period, pos_period = benchmark.GTA3a._pof_period, benchmark.GTA3a._pos_period
        period = int(pof_period*pos_period/fractions.gcd(pof_period, pos_period))
        for i in range(30):
            for n in range(benchmark.GTA3a.distinct_step*period):
                generation = n*benchmark.GTA3a.fixed_generation
                # additive
                solution = benchmark.GTA3a.solution_generate(generation)
                objectives = benchmark.GTA3a.objective_function(solution, generation)
                self.assertTrue(np.allclose(objectives[1], 1.0 - np.sqrt(objectives[0])))
                # multiplicative
                solution = benchmark.GTA3m.solution_generate(generation)
                objectives = benchmark.GTA3m.objective_function(solution, generation)
                self.assertTrue(np.allclose(objectives[1], 1.0 - np.sqrt(objectives[0])))


    def test_GTA4_solution_generate(self):
        pof_period, pos_period = benchmark.GTA4a._pof_period, benchmark.GTA4a._pos_period
        period = int(pof_period*pos_period/fractions.gcd(pof_period, pos_period))
        for i in range(30):
            for n in range(benchmark.GTA4a.distinct_step*period):
                generation = n*benchmark.GTA4a.fixed_generation
                # additive
                solution = benchmark.GTA4a.solution_generate(generation)
                objectives = benchmark.GTA4a.objective_function(solution, generation)
                self.assertTrue(np.allclose(objectives[1], 
                        1.5 - np.sqrt(objectives[0]) - 0.5*np.sin(10*np.pi*objectives[0])))
                # multiplicative
                solution = benchmark.GTA4m.solution_generate(generation)
                objectives = benchmark.GTA4m.objective_function(solution, generation)
                self.assertTrue(np.allclose(objectives[1], 
                        1.5 - np.sqrt(objectives[0]) - 0.5*np.sin(10*np.pi*objectives[0])))


    def test_GTA5_solution_generate(self):
        pof_period, pos_period = benchmark.GTA5a._pof_period, benchmark.GTA5a._pos_period
        period = int(pof_period*pos_period/fractions.gcd(pof_period, pos_period))
        for i in range(30):
            for n in range(benchmark.GTA5a.distinct_step*period):
                generation = n*benchmark.GTA5a.fixed_generation
                time = benchmark.GTA5a._generation_to_time(generation)
                k = benchmark.GTA5a._kindex(time)
                # additive
                solution = benchmark.GTA5a.solution_generate(generation)
                objectives = benchmark.GTA5a.objective_function(solution, generation)
                self.assertTrue(np.allclose(objectives[1], 
                        1.0 - np.sqrt(objectives[0]) + 0.1*k*(1 + np.sin(10*np.pi*objectives[0]))))
                # multiplicative
                solution = benchmark.GTA5m.solution_generate(generation)
                objectives = benchmark.GTA5m.objective_function(solution, generation)
                self.assertTrue(np.allclose(objectives[1], 
                        1.0 - np.sqrt(objectives[0]) + 0.1*k*(1 + np.sin(10*np.pi*objectives[0]))))


    def test_GTA6_solution_generate(self):
        pof_period, pos_period = benchmark.GTA6a._pof_period, benchmark.GTA6a._pos_period
        period = int(pof_period*pos_period/fractions.gcd(pof_period, pos_period))
        for i in range(30):
            for n in range(benchmark.GTA6a.distinct_step*period):
                generation = n*benchmark.GTA6a.fixed_generation
                time = benchmark.GTA6a._generation_to_time(generation)
                k = benchmark.GTA6a._kindex(time)
                # additive
                solution = benchmark.GTA6a.solution_generate(generation)
                objectives = benchmark.GTA6a.objective_function(solution, generation)
                self.assertTrue(np.allclose(objectives[1], 
                        1.0 - np.sqrt(objectives[0]) + 0.1*k*(1 + np.sin(10*np.pi*objectives[0]))))
                # multiplicative
                solution = benchmark.GTA6m.solution_generate(generation)
                objectives = benchmark.GTA6m.objective_function(solution, generation)
                self.assertTrue(np.allclose(objectives[1], 
                        1.0 - np.sqrt(objectives[0]) + 0.1*k*(1 + np.sin(10*np.pi*objectives[0]))))

                
    def test_GTA7_solution_generate(self):
        pof_period, pos_period = benchmark.GTA7a._pof_period, benchmark.GTA7a._pos_period
        period = int(pof_period*pos_period/fractions.gcd(pof_period, pos_period))
        for i in range(30):
            for n in range(benchmark.GTA7a.distinct_step*period):
                generation = n*benchmark.GTA7a.fixed_generation
                time = benchmark.GTA7a._generation_to_time(generation)
                k = benchmark.GTA7a._kindex(time)
                # additive
                solution = benchmark.GTA7a.solution_generate(generation)
                objectives = benchmark.GTA7a.objective_function(solution, generation)
                self.assertTrue(np.allclose(objectives[1], 
                        1 - np.power(objectives[0], np.log(1 - 0.1*k)/np.log(0.1*k + np.finfo(float).eps))))
                # multiplicative
                solution = benchmark.GTA7m.solution_generate(generation)
                objectives = benchmark.GTA7m.objective_function(solution, generation)
                self.assertTrue(np.allclose(objectives[1], 
                        1 - np.power(objectives[0], np.log(1 - 0.1*k)/np.log(0.1*k + np.finfo(float).eps))))


    def test_GTA8_solution_generate(self):
        pof_period, pos_period = benchmark.GTA8a._pof_period, benchmark.GTA8a._pos_period
        period = int(pof_period*pos_period/fractions.gcd(pof_period, pos_period))
        for i in range(30):
            for n in range(benchmark.GTA8a.distinct_step*period):
                generation = n*benchmark.GTA8a.fixed_generation
                time = benchmark.GTA8a._generation_to_time(generation)
                k = benchmark.GTA8a._kindex(time)
                # additive
                solution = benchmark.GTA8a.solution_generate(generation)
                objectives = benchmark.GTA8a.objective_function(solution, generation)
                self.assertTrue(np.allclose(objectives[1], 
                        1 - np.power(objectives[0], np.log(1 - 0.1*k)/np.log(0.1*k + np.finfo(float).eps))))
                # multiplicative
                solution = benchmark.GTA8m.solution_generate(generation)
                objectives = benchmark.GTA8m.objective_function(solution, generation)
                self.assertTrue(np.allclose(objectives[1], 
                        1 - np.power(objectives[0], np.log(1 - 0.1*k)/np.log(0.1*k + np.finfo(float).eps))))


    def test_GTA9_solution_generate(self):
        pof_period, pos_period = benchmark.GTA9a._pof_period, benchmark.GTA9a._pos_period
        period = int(pof_period*pos_period/fractions.gcd(pof_period, pos_period))
        for i in range(30):
            for n in range(benchmark.GTA9a.distinct_step*period):
                generation = n*benchmark.GTA9a.fixed_generation
                time = benchmark.GTA9a._generation_to_time(generation)
                k = benchmark.GTA9a._kindex(time)
                # additive
                solution = benchmark.GTA9a.solution_generate(generation)
                objectives = benchmark.GTA9a.objective_function(solution, generation)
                a, b = 0.5*solution[0]*np.pi, 0.5*solution[1]*np.pi
                self.assertTrue(np.allclose(objectives[0], np.cos(a)*np.cos(b)))
                self.assertTrue(np.allclose(objectives[1], np.cos(a)*np.sin(b)))
                self.assertTrue(np.allclose(objectives[2], np.sin(a + 0.25*(k/5.0)*np.pi)))
                # multiplicative
                solution = benchmark.GTA9m.solution_generate(generation)
                objectives = benchmark.GTA9m.objective_function(solution, generation)
                a, b = 0.5*solution[0]*np.pi, 0.5*solution[1]*np.pi
                self.assertTrue(np.allclose(objectives[0], np.cos(a)*np.cos(b)))
                self.assertTrue(np.allclose(objectives[1], np.cos(a)*np.sin(b)))
                self.assertTrue(np.allclose(objectives[2], np.sin(a + 0.25*(k/5.0)*np.pi)))


    def test_GTA10_solution_generate(self):
        pof_period, pos_period = benchmark.GTA10a._pof_period, benchmark.GTA10a._pos_period
        period = int(pof_period*pos_period/fractions.gcd(pof_period, pos_period))
        for i in range(30):
            for n in range(benchmark.GTA10a.distinct_step*period):
                generation = n*benchmark.GTA10a.fixed_generation
                time = benchmark.GTA10a._generation_to_time(generation)
                k = benchmark.GTA10a._kindex(time)
                # additive
                solution = benchmark.GTA10a.solution_generate(generation)
                objectives = benchmark.GTA10a.objective_function(solution, generation)
                a, b = 0.5*solution[0]*np.pi, 0.5*solution[1]*np.pi
                self.assertTrue(np.allclose(objectives[0], np.cos(a)*np.cos(b)))
                self.assertTrue(np.allclose(objectives[1], np.cos(a)*np.sin(b)))
                self.assertTrue(np.allclose(objectives[2], np.sin(a + 0.25*(k/5.0)*np.pi)))
                # multiplicative
                solution = benchmark.GTA10m.solution_generate(generation)
                objectives = benchmark.GTA10m.objective_function(solution, generation)
                a, b = 0.5*solution[0]*np.pi, 0.5*solution[1]*np.pi
                self.assertTrue(np.allclose(objectives[0], np.cos(a)*np.cos(b)))
                self.assertTrue(np.allclose(objectives[1], np.cos(a)*np.sin(b)))
                self.assertTrue(np.allclose(objectives[2], np.sin(a + 0.25*(k/5.0)*np.pi)))


    def test_GTA11_solution_generate(self):
        pof_period, pos_period = benchmark.GTA11a._pof_period, benchmark.GTA11a._pos_period
        period = int(pof_period*pos_period/fractions.gcd(pof_period, pos_period))
        for i in range(30):
            for n in range(benchmark.GTA11a.distinct_step*period):
                generation = n*benchmark.GTA11a.fixed_generation
                time = benchmark.GTA11a._generation_to_time(generation)
                k = benchmark.GTA11a._kindex(time)
                k_ratio = (5.0 - k)/5.0
                # additive
                solution = benchmark.GTA11a.solution_generate(generation)
                objectives = benchmark.GTA11a.objective_function(solution, generation)
                a, b = 0.5*solution[0]*np.pi, 0.5*solution[1]*np.pi*k_ratio
                self.assertTrue(np.allclose(objectives[0], np.cos(a)*np.cos(b)))
                self.assertTrue(np.allclose(objectives[1], np.cos(a)*np.sin(b)))
                self.assertTrue(np.allclose(objectives[2], np.sin(a + 0.25*(k/5.0)*np.pi)))
                # multiplicative
                solution = benchmark.GTA11m.solution_generate(generation)
                objectives = benchmark.GTA11m.objective_function(solution, generation)
                a, b = 0.5*solution[0]*np.pi, 0.5*solution[1]*np.pi*k_ratio
                self.assertTrue(np.allclose(objectives[0], np.cos(a)*np.cos(b)))
                self.assertTrue(np.allclose(objectives[1], np.cos(a)*np.sin(b)))
                self.assertTrue(np.allclose(objectives[2], np.sin(a + 0.25*(k/5.0)*np.pi)))


    def test_GTA12_solution_generate(self):
        pof_period, pos_period = benchmark.GTA12a._pof_period, benchmark.GTA12a._pos_period
        period = int(pof_period*pos_period/fractions.gcd(pof_period, pos_period))
        for i in range(30):
            for n in range(benchmark.GTA12a.distinct_step*period):
                generation = n*benchmark.GTA12a.fixed_generation
                time = benchmark.GTA12a._generation_to_time(generation)
                # additive
                k = benchmark.GTA12a._kindex(time)
                k_ratio = (5.0 - k)/5.0
                solution = benchmark.GTA12a.solution_generate(generation)
                objectives = benchmark.GTA12a.objective_function(solution, generation)
                a, b = 0.5*solution[0]*np.pi, 0.5*solution[1]*np.pi*k_ratio
                self.assertTrue(np.allclose(objectives[0], np.cos(a)*np.cos(b)))
                self.assertTrue(np.allclose(objectives[1], np.cos(a)*np.sin(b)))
                self.assertTrue(np.allclose(objectives[2], np.sin(a + 0.25*(k/5.0)*np.pi)))
                # multiplicative
                solution = benchmark.GTA12m.solution_generate(generation)
                objectives = benchmark.GTA12m.objective_function(solution, generation)
                a, b = 0.5*solution[0]*np.pi, 0.5*solution[1]*np.pi*k_ratio
                self.assertTrue(np.allclose(objectives[0], np.cos(a)*np.cos(b)))
                self.assertTrue(np.allclose(objectives[1], np.cos(a)*np.sin(b)))
                self.assertTrue(np.allclose(objectives[2], np.sin(a + 0.25*(k/5.0)*np.pi)))



if __name__ == '__main__':

    main()
