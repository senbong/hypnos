from __future__ import division

import os
import sys
from unittest import TestCase, main

# third-party package
import numpy as np

# add path to the root project
testdir = os.path.dirname(__file__)
sys.path.insert(0, os.path.join(testdir, '..'))

from hypnos.util import validation
from hypnos.metric import DistanceMetricSuite


class DistanceMetricSuiteTest(TestCase):

    def setUp(self):
        self.optimal_solution = np.array([[0.0, 1.0], [0.5, 0.5], [1.0, 0.0]])
        self.dmetric = DistanceMetricSuite()


    def test_update(self):
        dmetric = DistanceMetricSuite()
        with self.assertRaises(validation.NotUpdatedError):
            dmetric.inverted_generational_distance

        with self.assertRaises(validation.NotUpdatedError):
            dmetric.generational_distance

        with self.assertRaises(validation.NotUpdatedError):
            dmetric.average_hausdorff_distance


    def test_distance_metrics(self):
        approximate_solution = np.array([[0.5, 0.5]])
        self.dmetric.update(self.optimal_solution, approximate_solution)
        self.assertTrue(np.allclose(self.dmetric.generational_distance, 0.0))
        self.assertTrue(np.allclose(self.dmetric.inverted_generational_distance, np.sqrt(2.0)/3.0))
        self.assertTrue(np.allclose(self.dmetric.average_hausdorff_distance, 1.0/np.sqrt(3.0)))

        self.dmetric.update(self.optimal_solution, self.optimal_solution)
        self.assertTrue(np.allclose(self.dmetric.generational_distance, 0.0))
        self.assertTrue(np.allclose(self.dmetric.inverted_generational_distance, 0.0))
        self.assertTrue(np.allclose(self.dmetric.average_hausdorff_distance, 0.0))

        approximate_solution = np.array([[0.5, 1.0], [1.0, 0.5]])
        self.dmetric.update(self.optimal_solution, approximate_solution)
        self.assertTrue(np.allclose(self.dmetric.generational_distance, 0.5))
        self.assertTrue(np.allclose(self.dmetric.inverted_generational_distance, 0.5))
        self.assertTrue(np.allclose(self.dmetric.average_hausdorff_distance, 0.5))

        approximate_solution = np.array([[1.0, 1.0]])
        self.dmetric.update(self.optimal_solution, approximate_solution)
        self.assertTrue(np.allclose(self.dmetric.generational_distance, 1/np.sqrt(2.0)))
        self.assertTrue(np.allclose(self.dmetric.inverted_generational_distance, (2 + 1/np.sqrt(2))/3.0))
        self.assertTrue(np.allclose(self.dmetric.average_hausdorff_distance, np.sqrt(2.5)/np.sqrt(3)))



if __name__ == '__main__':

    main()
